﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class Klijent
    {
        public virtual int ID {  get; set;  }
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual string Sifra { get; set; }
        public virtual string E_mail { get; set; }
        public virtual string Ulica { get; set; }
        public virtual string BrojUUlici { get; set; }
        public virtual string Grad { get; set; }
        public virtual string BrojTelefona { get; set; }
        public virtual int JeKupac { get; set; }
        public virtual int JeVlasnik { get; set; }

        public virtual IList<Ugovor> UgovoriKupac { get; set; }
        public virtual IList<Nekretnina> NekretnineVlasnik { get; set; }

        public Klijent()
        {
            UgovoriKupac = new List<Ugovor>();
            NekretnineVlasnik = new List<Nekretnina>();
        }

        public override string ToString()
        {
            return ID + ". " + Ime + " " + Prezime + " " + E_mail;
        }
    }
}
