﻿using System;
using System.Collections.Generic;

namespace AgencijaZaNekretnine.Entiteti
{
    public abstract class Nekretnina
    {
        public virtual int Id { get; set; }
        public virtual string Ulica { get; set; }
        public virtual string BrojUUlici { get; set; }
        public virtual string GradLokacija { get; set; }
        public virtual string Tip { get; set; }
        public virtual int Kvadratura { get; set; }
        public virtual string BrojKatastarskeParcele { get; set; }
        public virtual string KatastarskaOpstina { get; set; }
        public virtual int Cena { get; set; }
        public virtual DateTime DatumIzgradnje { get; set; }
        public virtual int Spratnost { get; set; }
        public virtual string KratakOpis { get; set; }
        public virtual string IdFizickogUgovora { get; set; }
        public virtual string Tip_Za_Iznajmljivanje { get; set; }
        public virtual string ZaProdaju { get; set; }

        public virtual Agent PripadaAgentu { get; set; }

        public virtual Klijent PripadaVlasniku { get; set; }

        public virtual IList<Fotografija> Fotografije { get;set;}

        public Nekretnina()
        {
            Fotografije = new List<Fotografija>();
        }

    }
}