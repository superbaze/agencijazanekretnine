﻿using AgencijaZaNekretnine.Forme;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine
{
    public partial class LoginRegistracijaForm : Form
    {
        public LoginRegistracijaForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            (new LoginForm(Korisnik.Klijent)).Show();
            this.Hide();
        }

        private void btnRegistracija_Click(object sender, EventArgs e)
        {
            (new RegistracijaForm()).Show();
            this.Hide();
        }
    }
}
