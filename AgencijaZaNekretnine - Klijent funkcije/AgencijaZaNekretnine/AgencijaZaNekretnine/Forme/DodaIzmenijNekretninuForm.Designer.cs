﻿namespace AgencijaZaNekretnine.Forme
{
    partial class DodaIzmenijNekretninuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxTip = new System.Windows.Forms.ComboBox();
            this.cbxVrstaIznajmljivanja = new System.Windows.Forms.ComboBox();
            this.lblOd = new System.Windows.Forms.Label();
            this.lblDo = new System.Windows.Forms.Label();
            this.dtpOd = new System.Windows.Forms.DateTimePicker();
            this.dtpDo = new System.Windows.Forms.DateTimePicker();
            this.nudMaxMeseci = new System.Windows.Forms.NumericUpDown();
            this.txtIznajmljivac = new System.Windows.Forms.TextBox();
            this.lblIznajmljivac = new System.Windows.Forms.Label();
            this.lblMaxMeseci = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.nekretninaKontrol = new AgencijaZaNekretnine.Forme.NekretninaKontrol();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxMeseci)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxTip
            // 
            this.cbxTip.FormattingEnabled = true;
            this.cbxTip.Items.AddRange(new object[] {
            "Iznajmljivanje",
            "Prodaja"});
            this.cbxTip.Location = new System.Drawing.Point(14, 458);
            this.cbxTip.Name = "cbxTip";
            this.cbxTip.Size = new System.Drawing.Size(121, 21);
            this.cbxTip.TabIndex = 1;
            this.cbxTip.SelectedIndexChanged += new System.EventHandler(this.cbxTip_SelectedIndexChanged);
            // 
            // cbxVrstaIznajmljivanja
            // 
            this.cbxVrstaIznajmljivanja.FormattingEnabled = true;
            this.cbxVrstaIznajmljivanja.Items.AddRange(new object[] {
            "Kraci period",
            "Duzi period"});
            this.cbxVrstaIznajmljivanja.Location = new System.Drawing.Point(14, 487);
            this.cbxVrstaIznajmljivanja.Name = "cbxVrstaIznajmljivanja";
            this.cbxVrstaIznajmljivanja.Size = new System.Drawing.Size(121, 21);
            this.cbxVrstaIznajmljivanja.TabIndex = 2;
            this.cbxVrstaIznajmljivanja.SelectedIndexChanged += new System.EventHandler(this.cbxVrstaIznajmljivanja_SelectedIndexChanged);
            // 
            // lblOd
            // 
            this.lblOd.AutoSize = true;
            this.lblOd.Location = new System.Drawing.Point(162, 462);
            this.lblOd.Name = "lblOd";
            this.lblOd.Size = new System.Drawing.Size(22, 13);
            this.lblOd.TabIndex = 3;
            this.lblOd.Text = "od:";
            // 
            // lblDo
            // 
            this.lblDo.AutoSize = true;
            this.lblDo.Location = new System.Drawing.Point(162, 491);
            this.lblDo.Name = "lblDo";
            this.lblDo.Size = new System.Drawing.Size(22, 13);
            this.lblDo.TabIndex = 4;
            this.lblDo.Text = "do:";
            // 
            // dtpOd
            // 
            this.dtpOd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOd.Location = new System.Drawing.Point(215, 458);
            this.dtpOd.Name = "dtpOd";
            this.dtpOd.Size = new System.Drawing.Size(119, 20);
            this.dtpOd.TabIndex = 5;
            // 
            // dtpDo
            // 
            this.dtpDo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDo.Location = new System.Drawing.Point(215, 487);
            this.dtpDo.Name = "dtpDo";
            this.dtpDo.Size = new System.Drawing.Size(119, 20);
            this.dtpDo.TabIndex = 6;
            // 
            // nudMaxMeseci
            // 
            this.nudMaxMeseci.Location = new System.Drawing.Point(215, 458);
            this.nudMaxMeseci.Name = "nudMaxMeseci";
            this.nudMaxMeseci.Size = new System.Drawing.Size(119, 20);
            this.nudMaxMeseci.TabIndex = 7;
            // 
            // txtIznajmljivac
            // 
            this.txtIznajmljivac.Location = new System.Drawing.Point(215, 487);
            this.txtIznajmljivac.Name = "txtIznajmljivac";
            this.txtIznajmljivac.Size = new System.Drawing.Size(119, 20);
            this.txtIznajmljivac.TabIndex = 8;
            // 
            // lblIznajmljivac
            // 
            this.lblIznajmljivac.AutoSize = true;
            this.lblIznajmljivac.Location = new System.Drawing.Point(146, 491);
            this.lblIznajmljivac.Name = "lblIznajmljivac";
            this.lblIznajmljivac.Size = new System.Drawing.Size(64, 13);
            this.lblIznajmljivac.TabIndex = 9;
            this.lblIznajmljivac.Text = "Iznajmljivac:";
            // 
            // lblMaxMeseci
            // 
            this.lblMaxMeseci.AutoSize = true;
            this.lblMaxMeseci.Location = new System.Drawing.Point(144, 462);
            this.lblMaxMeseci.Name = "lblMaxMeseci";
            this.lblMaxMeseci.Size = new System.Drawing.Size(65, 13);
            this.lblMaxMeseci.TabIndex = 10;
            this.lblMaxMeseci.Text = "max meseci:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(461, 486);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 11;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(380, 486);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // nekretninaKontrol
            // 
            this.nekretninaKontrol.Location = new System.Drawing.Point(0, 0);
            this.nekretninaKontrol.Name = "nekretninaKontrol";
            this.nekretninaKontrol.Nekretnina = null;
            this.nekretninaKontrol.Size = new System.Drawing.Size(574, 454);
            this.nekretninaKontrol.TabIndex = 13;
            // 
            // DodaIzmenijNekretninuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 521);
            this.Controls.Add(this.nekretninaKontrol);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblMaxMeseci);
            this.Controls.Add(this.lblIznajmljivac);
            this.Controls.Add(this.txtIznajmljivac);
            this.Controls.Add(this.nudMaxMeseci);
            this.Controls.Add(this.dtpDo);
            this.Controls.Add(this.dtpOd);
            this.Controls.Add(this.lblDo);
            this.Controls.Add(this.lblOd);
            this.Controls.Add(this.cbxVrstaIznajmljivanja);
            this.Controls.Add(this.cbxTip);
            this.Name = "DodaIzmenijNekretninuForm";
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxMeseci)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbxTip;
        private System.Windows.Forms.ComboBox cbxVrstaIznajmljivanja;
        private System.Windows.Forms.Label lblOd;
        private System.Windows.Forms.Label lblDo;
        private System.Windows.Forms.DateTimePicker dtpOd;
        private System.Windows.Forms.DateTimePicker dtpDo;
        private System.Windows.Forms.NumericUpDown nudMaxMeseci;
        private System.Windows.Forms.TextBox txtIznajmljivac;
        private System.Windows.Forms.Label lblIznajmljivac;
        private System.Windows.Forms.Label lblMaxMeseci;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private NekretninaKontrol nekretninaKontrol;
    }
}