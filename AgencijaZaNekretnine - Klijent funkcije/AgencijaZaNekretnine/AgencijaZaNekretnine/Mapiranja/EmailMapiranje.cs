﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    public class EmailMapiranje: ClassMap<Email>
    {
        public EmailMapiranje()
        {
            Table("EMAIL");

            CompositeId()
                .KeyReference(x => x.IdAgenta,"ID_AGENTA")
                .KeyProperty(x => x.E_mail,"EMAIL");
        }
    }
}
