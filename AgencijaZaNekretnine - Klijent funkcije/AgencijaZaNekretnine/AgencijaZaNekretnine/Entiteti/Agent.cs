﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class Agent:Zaposleni
    {
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual int RadniStaz { get; set; }

        public virtual IList<Nekretnina> Nekretnine { get; set; }
        public virtual IList<Ugovor> Ugovori { get; set;  }
        public virtual IList<Email> Email { get; set; }
        public virtual IList<BrojTelefona> BrojeviTelefona { get; set; }

        public Agent()
        {
            Nekretnine = new List<Nekretnina>();
            Ugovori = new List<Ugovor>();
            Email = new List<Email>();
            BrojeviTelefona = new List<BrojTelefona>();
        }

        public override string ToString()
        {
            return Id + ".  " + Ime + " " + Prezime;
        }
    }
}
