﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class UgovorMapiranja : ClassMap<Ugovor>
    {
        public UgovorMapiranja()
        {
            Table("UGOVOR");

            Id(x => x.Broj, "BROJ").GeneratedBy.TriggerIdentity();

            Map(x => x.DatumPotpisivanja, "DATUM_POTPISIVANJA");
            Map(x => x.NaknadaZaPosredovanje, "NAKNADA_ZA_POSREDOVANJE");
            Map(x => x.BonusZaAgenta, "BONUS_ZA_AGENTA");

            References(x => x.PripadaAgentu).Column("ID_AGENTA").LazyLoad();
            References(x => x.Kupac).Column("ID_KUPCA").LazyLoad();
            References(x => x.ZastupnikVlasnika).Column("ID_ZASTUPNIK_VLASNIK").LazyLoad();
            References(x => x.ZastupnikKupca).Column("ID_ZASTUPNIK_KUPAC").LazyLoad();
        }
    }
}
