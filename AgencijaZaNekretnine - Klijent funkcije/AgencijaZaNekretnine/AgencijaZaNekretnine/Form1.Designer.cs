﻿namespace AgencijaZaNekretnine
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.btnDodajUgovorIznajmljivanje = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 15);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(293, 27);
            this.button1.TabIndex = 0;
            this.button1.Text = "MAGNETO - Pull Everything";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.LoadFromDB_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 84);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(295, 30);
            this.button2.TabIndex = 1;
            this.button2.Text = "Dodaj Fotografiju";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.DodajFotografiju_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(15, 155);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(295, 32);
            this.button3.TabIndex = 2;
            this.button3.Text = "Dodaj Nekretninu Za Prodaju";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.DodajNekretninaZaProdaju_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(15, 192);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(295, 33);
            this.button4.TabIndex = 3;
            this.button4.Text = "Dodaj Nekretninu Za Iznajmljivanje - Kraci";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.DodajNekretninaKraci_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(15, 230);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(295, 31);
            this.button5.TabIndex = 4;
            this.button5.Text = "Dodaj Nekretninu Za Iznajmljivanje - Duzi";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.DodajNekretninaDuzi_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(15, 373);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(295, 31);
            this.button8.TabIndex = 7;
            this.button8.Text = "Obrisi Kreirano";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.ObrisiKreirano_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(15, 48);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(295, 31);
            this.button9.TabIndex = 8;
            this.button9.Text = "Dodaj Agenta";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.DodajAgenta_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(15, 266);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(295, 31);
            this.button10.TabIndex = 9;
            this.button10.Text = "Dodaj Mail";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.DodajMail_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(15, 302);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(295, 31);
            this.button11.TabIndex = 10;
            this.button11.Text = "Dodaj Telefon";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.DodajTelefon_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(15, 118);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(295, 32);
            this.button12.TabIndex = 11;
            this.button12.Text = "Dodaj Pravnog Zastupnika";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.DodajPravniZastupnik_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(15, 337);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(295, 31);
            this.button13.TabIndex = 12;
            this.button13.Text = "Dodaj Klijenta";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.DodajKlijenta_Click);
            // 
            // btnDodajUgovorIznajmljivanje
            // 
            this.btnDodajUgovorIznajmljivanje.Location = new System.Drawing.Point(16, 439);
            this.btnDodajUgovorIznajmljivanje.Name = "btnDodajUgovorIznajmljivanje";
            this.btnDodajUgovorIznajmljivanje.Size = new System.Drawing.Size(293, 23);
            this.btnDodajUgovorIznajmljivanje.TabIndex = 13;
            this.btnDodajUgovorIznajmljivanje.Text = "Test dodaj ugovor iznajmljivnaje";
            this.btnDodajUgovorIznajmljivanje.UseVisualStyleBackColor = true;
            this.btnDodajUgovorIznajmljivanje.Click += new System.EventHandler(this.btnDodajUgovorIznajmljivanje_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 529);
            this.Controls.Add(this.btnDodajUgovorIznajmljivanje);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agencija Za Nekretnine";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button btnDodajUgovorIznajmljivanje;
    }
}

