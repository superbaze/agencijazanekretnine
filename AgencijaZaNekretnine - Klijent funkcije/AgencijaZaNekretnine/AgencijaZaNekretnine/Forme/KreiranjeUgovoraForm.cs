﻿using AgencijaZaNekretnine.DTOs;
using AgencijaZaNekretnine.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine.Forme
{
    public partial class KreiranjeUgovoraForm : Form
    {
        public Tip TipUgovora { get; private set; }
        public DateTime DatumPotpisivanja { get; private set; }
        public DateTime DatumOd { get; private set; }
        public DateTime DatumDo { get; private set; }
        public float NadoknadaPosredovanje { get; private set; }
        public float BonusAgent { get; private set; }
        public float MesecnaRata { get; private set; }
        public float OstvarenaCena { get; private set; }
        public float NaknadaZaNotara { get; private set; }
        public string ImeNotara { get; private set; }

        public int NekretninaID { get; private set; }
        public int PravniZastupnikKupac { get; private set; }
        public int PravniZastupnikProdavac { get; private set; }
        public int KupacID { get; private set; }

        private int AgentID { get; set; }
        private List<NekretninaOsnovneInformacije> agentNekretnine;

        public KreiranjeUgovoraForm()
        {
            InitializeComponent();
        }
        public KreiranjeUgovoraForm(int idAgenta):this()
        {
            AgentID = idAgenta;
            agentNekretnine = DTOManager.VratiNekretnineAgenta(AgentID);
        }

        

        #region Methods

        private void Switch()
        {
            if ((Tip)cbxTip.SelectedItem == Tip.Iznajmljivanje)
            {
                gbxIznajmljivanje.Visible = true;
                gbxProdaja.Visible = false;
                listboxNekretnine.Items.Clear();
                //listboxNekretnine.Items.AddRange(DTOManager.VratiNekretnineIznajmljene().ToArray());
                foreach(NekretninaOsnovneInformacije n in agentNekretnine)
                {
                    if (n.TipNekretnine.Contains("iznajmljivanje"))
                        listboxNekretnine.Items.Add(n);
                }
            }
            else
            {
                gbxProdaja.Parent = this;
                gbxIznajmljivanje.Visible = false;
                gbxProdaja.Visible = true;
                gbxProdaja.Location = gbxIznajmljivanje.Location;
                listboxNekretnine.Items.Clear();
                foreach (NekretninaOsnovneInformacije n in agentNekretnine)
                {
                    if (n.TipNekretnine.Contains("prodaja"))
                        listboxNekretnine.Items.Add(n);
                }
            }
        }

        #endregion

        #region EventHandlers

        private void KreiranjeUgovoraForm_Load(object sender, EventArgs e)
        {
            cbxTip.DataSource = Enum.GetValues(typeof(Tip));
            cbxKupac.DataSource = DTOManager.VratiSveKlijente();
            cbxPZKupac.DataSource = DTOManager.VratiPravneZastupnike();
            cbxPZProdavac.DataSource = DTOManager.VratiPravneZastupnike();
            lblDatumPotpisivanjaA.Text = DateTime.Now.ToShortDateString();
            DatumPotpisivanja = DateTime.Now;
        }

        private void cbxTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            Switch();
        }

        #region KeyPress

        private void cbxBezZastupnika_CheckedChanged(object sender, EventArgs e)
        {
            if(cbxBezZastupnika.Checked)
            {
                cbxPZKupac.Enabled = false;
                cbxPZProdavac.Enabled = false;
            }
            else
            {
                cbxPZKupac.Enabled = true;
                cbxPZProdavac.Enabled = true;
            }
        }

        private void tbxNadoknadaPos_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if(ch == 46 && 
                (tbxNadoknadaPos.Text.IndexOf((char)46) != -1 || tbxNadoknadaPos.Text.Length == 0))
            {
                e.Handled = true;
                return;
            }
            if(!Char.IsDigit(ch) && ch!=8 && ch!=46 && ch!=45)
            {
                e.Handled = true;
            }
        }

        private void tbxBonusAgent_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 &&
                (tbxBonusAgent.Text.IndexOf((char)46) != -1 || tbxBonusAgent.Text.Length == 0))
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46 && ch != 45)
            {
                e.Handled = true;
            }
        }

        private void tbxMesecnaRata_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 &&
                (tbxMesecnaRata.Text.IndexOf((char)46) != -1 || tbxMesecnaRata.Text.Length == 0))
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46 && ch != 45)
            {
                e.Handled = true;
            }
        }

        private void tbxOstvarenaCena_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 &&
                (tbxOstvarenaCena.Text.IndexOf((char)46) != -1 || tbxOstvarenaCena.Text.Length == 0))
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46 && ch != 45)
            {
                e.Handled = true;
            }
        }

        private void tbxNaknadaNotar_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 &&
                (tbxNaknadaNotar.Text.IndexOf((char)46) != -1 || tbxNaknadaNotar.Text.Length == 0))
            {
                e.Handled = true;
                return;
            }
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46 && ch != 45)
            {
                e.Handled = true;
            }
        }

        #endregion

        #region Buttons

        //fali nekretnina
        private void btnOK_Click(object sender, EventArgs e)
        {
            TipUgovora = (Tip)cbxTip.SelectedItem;
            NadoknadaPosredovanje = float.Parse(tbxNadoknadaPos.Text);
            BonusAgent = float.Parse(tbxBonusAgent.Text);
            Klijent k = (Klijent)cbxKupac.SelectedItem;
            KupacID = k.ID;
            if(!cbxBezZastupnika.Checked)
            {
                PravniZastupnik z = (PravniZastupnik)cbxPZKupac.SelectedItem;
                PravniZastupnikKupac = z.ID;
                z = (PravniZastupnik)cbxPZProdavac.SelectedItem;
                PravniZastupnikProdavac = z.ID;
            }
            else
            {
                PravniZastupnikKupac = 0;
                PravniZastupnikProdavac = 0;
            }
            if(TipUgovora == Tip.Iznajmljivanje)
            {
                DatumDo = dtpDo.Value;
                DatumOd = dtpOd.Value;
                MesecnaRata = float.Parse(tbxMesecnaRata.Text);
            }
            else
            {
                ImeNotara = tbxImeNotara.Text;
                NaknadaZaNotara = float.Parse(tbxNaknadaNotar.Text);
                OstvarenaCena = float.Parse(tbxOstvarenaCena.Text);
            }

        }

        #endregion

        #endregion
    }

    public enum Tip
    {
        Iznajmljivanje,
        Prodaja
    }
}
