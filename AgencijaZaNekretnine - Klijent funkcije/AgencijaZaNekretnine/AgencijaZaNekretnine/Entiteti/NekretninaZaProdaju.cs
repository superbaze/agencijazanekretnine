﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class NekretninaZaProdaju: Nekretnina
    {
        public virtual UgovorZaProdaju Ugovor { get; set; }

        public NekretninaZaProdaju()
        {
            this.ZaProdaju = "da";
        }
    }
}
