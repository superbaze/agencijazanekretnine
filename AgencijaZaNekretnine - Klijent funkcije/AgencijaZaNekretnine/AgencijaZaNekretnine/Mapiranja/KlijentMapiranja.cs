﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class KlijentMapiranja : ClassMap<Klijent>
    {
        public KlijentMapiranja()
        {
            Table("KLIJENT");

            Id(x => x.ID).GeneratedBy.TriggerIdentity();

            Map(x => x.Ime, "IME");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.E_mail, "EMAIL");
            Map(x => x.Sifra, "SIFRA");
            Map(x => x.Ulica, "ULICA");
            Map(x => x.BrojUUlici, "BROJ_U_ULICI");
            Map(x => x.Grad, "GRAD");
            Map(x => x.BrojTelefona, "BROJ_TELEFONA");
            Map(x => x.JeKupac, "JEKUPAC");
            Map(x => x.JeVlasnik, "JEVLASNIK");

            HasMany(x => x.UgovoriKupac).KeyColumn("ID_KUPCA").LazyLoad().Inverse().Cascade.All();
            HasMany(x => x.NekretnineVlasnik).KeyColumn("ID_VLASNIKA").LazyLoad().Inverse().Cascade.All();
        }
    }
}
