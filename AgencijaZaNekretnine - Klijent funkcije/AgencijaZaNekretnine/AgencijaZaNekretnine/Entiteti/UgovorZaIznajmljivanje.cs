﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class UgovorZaIznajmljivanje : Ugovor
    {
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }
        public virtual float MesecnaRenta { get; set; }
        public virtual NekretninaZaIznajmljivanje PripadaNekretnini{ get; set; } 
    }
}
