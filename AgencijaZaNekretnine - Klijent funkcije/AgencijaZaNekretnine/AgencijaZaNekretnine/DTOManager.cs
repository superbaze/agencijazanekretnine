﻿using AgencijaZaNekretnine.DTOs;
using AgencijaZaNekretnine.Entiteti;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine
{
    public class DTOManager
    {
        #region KLIJENT FUNKCIJE
        public static Klijent LogIn(string email, string sifra)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                IList<Klijent> klijent = s.QueryOver<Klijent>()
                    .Where(x => x.E_mail == email)
                    .Where(x => x.Sifra == sifra)
                    .List<Klijent>();
                s.Close();
                if (klijent.Count != 0)
                    return klijent[0];
                return null;

            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                return null;
            }

        }

        public static Agent LogInAgent(string ime, string sifra)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                IList<Agent> agenti = s.QueryOver<Agent>()
                    .Where(x => x.Ime == ime)
                    .Where(x => x.Sifra == sifra)
                    .List<Agent>();
                s.Close();
                if (agenti.Count != 0)
                    return agenti[0];
                return null;
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public static Klijent Register(string ime, string prezime,
            string sifra, string email, string ulica, string brojUUlici,
            string grad, string brojTelefon)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Klijent k = new Klijent();
                k.BrojTelefona = brojTelefon;
                k.BrojUUlici = brojUUlici;
                k.E_mail = email;
                k.Grad = grad;
                k.Ime = ime;
                k.Prezime = prezime;
                k.JeKupac = 1;
                k.Sifra = sifra;
                k.Ulica = ulica;

                s.Save(k);

                s.Flush();
                s.Close();
                return k;
                

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<Klijent> VratiSveKlijente()
        {
            try
            {
                ISession s = DataLayer.GetSession();
                IList<Klijent> klijent = s.QueryOver<Klijent>().List<Klijent>();
                s.Close();
                return klijent.ToList();


            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<NekretninaOsnovneInformacije> VratiSveNekretnine()
        {
            try
            {
                ISession s = DataLayer.GetSession();
                List<NekretninaOsnovneInformacije> nekretnineOsn = new List<NekretninaOsnovneInformacije>();
                IList<Nekretnina> nekretnina = s.QueryOver<Nekretnina>().List<Nekretnina>();

                foreach(Nekretnina n in nekretnina)
                {
                    if (n.PripadaVlasniku != null)
                    {
                        nekretnineOsn.Add(new NekretninaOsnovneInformacije(n));
                    }
                }
                s.Close();
                return nekretnineOsn;


            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<NekretninaOsnovneInformacije> VratiNekretnineVlasnik(int idVlasnika)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                List<NekretninaOsnovneInformacije> nekretnineOsn = new List<NekretninaOsnovneInformacije>();
                IList<Nekretnina> nekretnina = s.QueryOver<Nekretnina>()
                    .Where(x => x.PripadaVlasniku.ID == idVlasnika).List<Nekretnina>();

                foreach (Nekretnina n in nekretnina)
                {
                    if (n.PripadaVlasniku != null)
                    {
                        nekretnineOsn.Add(new NekretninaOsnovneInformacije(n));
                    }
                }
                s.Close();
                return nekretnineOsn;


            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<NekretninaOsnovneInformacije> VratiNekretnineIznajmljene(int idKlijenta)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                List<NekretninaOsnovneInformacije> nekretnineOsn = new List<NekretninaOsnovneInformacije>();
                Klijent klijent = s.QueryOver<Klijent>().Where(x => x.ID == idKlijenta).List<Klijent>()[0];

                foreach (Ugovor ugovor in klijent.UgovoriKupac)
                {
                    if(typeof(UgovorZaIznajmljivanje) == ugovor.GetType())
                    {
                        NekretninaZaIznajmljivanje nekretnina = (NekretninaZaIznajmljivanje)(((UgovorZaIznajmljivanje)ugovor).PripadaNekretnini);
                        nekretnineOsn.Add(new NekretninaOsnovneInformacije(nekretnina));
                    }
                }
                s.Close();
                return nekretnineOsn;


            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion

        public static void KreirajUgovorZaIznajmljivanje(DateTime datumPotpisivanja, float naknadaZaPosredovanje,
            float bonusZaAgenta, int agentId,  int kupacId, int nekretninaId, 
            DateTime datumDo, DateTime datumOd, float mesecnaRenta=0, int zastupnikVlasnikId = 0 ,
            int zastupnikKupacId = 0 ) // kreira novi ugovor za datog agenta
        {
            ISession session = null;
            try
            {
                UgovorZaIznajmljivanje ugovorIznajmljivanje = new UgovorZaIznajmljivanje();
                ugovorIznajmljivanje.DatumPotpisivanja = datumPotpisivanja;
                ugovorIznajmljivanje.NaknadaZaPosredovanje = naknadaZaPosredovanje;
                ugovorIznajmljivanje.BonusZaAgenta = bonusZaAgenta;
                ugovorIznajmljivanje.DatumOd = datumOd;
                ugovorIznajmljivanje.DatumDo = datumDo;
                ugovorIznajmljivanje.MesecnaRenta = mesecnaRenta;


                session = DataLayer.GetSession();
                PravniZastupnik zastupnikVlasnik = null, zastupnikKupac = null;
                Agent agent = session.Get<Agent>(agentId);
                Klijent kupac = session.Get<Klijent>(kupacId);
                if (zastupnikVlasnikId != 0)
                    zastupnikVlasnik = session.Get<PravniZastupnik>(zastupnikVlasnikId);
                if (zastupnikKupacId != 0)
                    zastupnikKupac = session.Get<PravniZastupnik>(zastupnikKupacId);
                NekretninaZaIznajmljivanje nekretninaZaIznajmljvanje = session.Get<NekretninaZaIznajmljivanje>(nekretninaId);

                ugovorIznajmljivanje.ZastupnikVlasnika = zastupnikVlasnik;
                ugovorIznajmljivanje.ZastupnikKupca = zastupnikKupac;
                ugovorIznajmljivanje.PripadaAgentu = agent;
                ugovorIznajmljivanje.Kupac = kupac;
                ugovorIznajmljivanje.PripadaNekretnini = nekretninaZaIznajmljvanje;

                session.Save(ugovorIznajmljivanje);
                session.Flush();
                session.Close();
            }
            catch(Exception ex)
            {
                if(session!=null)
                    session.Close();
            }
            
        }

        public static void KreirajUgovorZaProdaju(DateTime datumPotpisivanja, float naknadaZaPosredovanje,
            float bonusZaAgenta, int agentId, int kupacId, float ostvarenaCena, float naknadaZaNotara,
            string imeNotara, int nekretninaId, int zastupnikVlasnikId = 0, int zastupnikKupacId = 0 )
        {
            ISession session = null;
            try
            {
                UgovorZaProdaju ugovorProdaja = new UgovorZaProdaju();
                ugovorProdaja.DatumPotpisivanja = datumPotpisivanja;
                ugovorProdaja.NaknadaZaPosredovanje = naknadaZaPosredovanje;
                ugovorProdaja.BonusZaAgenta = bonusZaAgenta;
                ugovorProdaja.ImeNotara = imeNotara;
                ugovorProdaja.OstvarenaCena = ostvarenaCena;
                ugovorProdaja.NaknadaZaNotara = naknadaZaNotara;

                session = DataLayer.GetSession();
                PravniZastupnik zastupnikVlasnik = null, zastupnikKupac = null;
                Agent agent = session.Get<Agent>(agentId);
                Klijent kupac = session.Get<Klijent>(kupacId);
                if (zastupnikVlasnikId != 0)
                    zastupnikVlasnik = session.Get<PravniZastupnik>(zastupnikVlasnikId);
                if (zastupnikKupacId != 0)
                    zastupnikKupac = session.Get<PravniZastupnik>(zastupnikKupacId);
                NekretninaZaProdaju nekretninaZaProdaju = session.Get<NekretninaZaProdaju>(nekretninaId);

                ugovorProdaja.ZastupnikVlasnika = zastupnikVlasnik;
                ugovorProdaja.ZastupnikKupca = zastupnikKupac;
                ugovorProdaja.PripadaAgentu = agent;
                ugovorProdaja.Kupac = kupac;
                ugovorProdaja.Prodaja = nekretninaZaProdaju;

                session.Save(ugovorProdaja);
                session.Flush();
                session.Close();
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
        }

        public static List<UgovorDTO> VratiUgovoreAgenta(int agentId)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                IList<Ugovor> ugovori = session.QueryOver<Ugovor>()
                    .Where(x => x.PripadaAgentu.Id == agentId).List<Ugovor>();
                List<UgovorDTO> ugovoriDto = new List<UgovorDTO>(ugovori.Count);
                foreach(Ugovor u in ugovori)
                {
                    ugovoriDto.Add(new UgovorDTO(u));
                }

                session.Close();
                return ugovoriDto;
            }
            catch(Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return null;
        }

        public static List<NekretninaOsnovneInformacije> VratiNekretnineAgenta(int agentId)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                IList<Nekretnina> nekretnine = session.QueryOver<Nekretnina>()
                    .Where(x => x.PripadaAgentu.Id == agentId).List<Nekretnina>();
                List<NekretninaOsnovneInformacije> nekretnineDto = new List<NekretninaOsnovneInformacije>(nekretnine.Count);
                foreach(Nekretnina n in nekretnine)
                {
                    nekretnineDto.Add(new NekretninaOsnovneInformacije(n));
                }

                session.Close();
                return nekretnineDto;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return null;
        }

        public bool DodajAgenta(string sifra, string ime,
            string prezime, int radniStaz)
        {
            ISession session = null;
            try
            {
                Agent agent = new Agent();
                agent.Sifra = sifra;
                agent.Ime = ime;
                agent.Prezime = prezime;
                agent.RadniStaz = radniStaz;
                session = DataLayer.GetSession();

                session.Save(agent);
                session.Flush();

                session.Close();
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static List<Agent> VratiAgente()
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                IList<Agent> agenti = session.QueryOver<Agent>()
                    .List<Agent>();
                session.Close();
                return agenti.ToList();
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return null;
        }

        public List<Ugovor> VratiUgovore()
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                IList<Ugovor> nekretine = session.QueryOver<Ugovor>()
                    .List<Ugovor>();
                session.Close();
                return nekretine.ToList();
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return null;
        }

        public List<Klijent> VratiKlijente()
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                IList<Klijent> klijent = session.QueryOver<Klijent>()
                    .List<Klijent>();

                session.Close();
                return klijent.ToList();
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return null;
        }

        public static bool UpdateNekretnina(Nekretnina nekretnina) // nije implementirana, ne znam da li moze ovako ili treba zasebna fja za svaku vrstu??
        {
            return true;
        }

        public static bool UpdateAgent(Agent agent)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                session.Update(agent);

                session.Flush();
                session.Close();
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static bool UpdateKlijent(Klijent klijent)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                session.Update(klijent);

                session.Flush();
                session.Close();
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static bool UpdatePravniZastupnik(PravniZastupnik zastupnik)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                session.Update(zastupnik);

                session.Flush();
                session.Close();
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static List<PravniZastupnik> VratiPravneZastupnike()
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                IList<PravniZastupnik> list = session.QueryOver<PravniZastupnik>()
                    .List<PravniZastupnik>();
                session.Close();
                return list.ToList();
            }
            catch(Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return null;
        }

        public static bool UpdateUgovor(Ugovor ugovor)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                session.Update(ugovor);

                session.Flush();
                session.Close();
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        //zasto je cena int kod nekretnine??
        public static bool DodajNekretninuZaProdaju(string ulica, string brojUlice, string gradLokacija, string tip,
                            int kvadratura, string brojKatastarskeParcele, string katastarskaOpstina,
                            int cena, DateTime datumIzgradnje, int spratnost, string kratakOpis, int agentId,
                            int vlasnikId)
        {
            ISession session = null;
            try
            {
                NekretninaZaProdaju nekretnina = new NekretninaZaProdaju();
                nekretnina.Ulica = ulica;
                nekretnina.BrojUUlici = brojUlice;
                nekretnina.GradLokacija = gradLokacija;
                nekretnina.Tip = tip; //"prodaja";
                nekretnina.Kvadratura = kvadratura;
                nekretnina.BrojKatastarskeParcele = brojKatastarskeParcele;
                nekretnina.KatastarskaOpstina = katastarskaOpstina;
                nekretnina.Cena = cena;
                nekretnina.DatumIzgradnje = datumIzgradnje;
                nekretnina.Spratnost = spratnost;
                nekretnina.KratakOpis = kratakOpis;

                session = DataLayer.GetSession();
                Agent agent = session.Get<Agent>(agentId);
                Klijent vlasnik = session.Get<Klijent>(vlasnikId);
                nekretnina.PripadaAgentu = agent;
                nekretnina.PripadaVlasniku = vlasnik;

                session.Flush();
                session.Save(nekretnina);
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static bool DodajNekretninuZaIznajmljivanjeKraci(string ulica, string brojUlice, string gradLokacija, string tip,
                            int kvadratura, string brojKatastarskeParcele, string katastarskaOpstina,
                            int cena, DateTime datumIzgradnje, int spratnost, string kratakOpis, int agentId,
                            int vlasnikId, DateTime datumOd, DateTime datumDo, string tipZaIznajmljivnje = "kraci period")
        {
            ISession session = null;
            try
            {
                NekretninaNaKraciPeriod nekretnina = new NekretninaNaKraciPeriod();
                nekretnina.Ulica = ulica;
                nekretnina.BrojUUlici = brojUlice;
                nekretnina.GradLokacija = gradLokacija;
                nekretnina.Tip = tip;//"iznajmljivanje";
                nekretnina.Kvadratura = kvadratura;
                nekretnina.BrojKatastarskeParcele = brojKatastarskeParcele;
                nekretnina.KatastarskaOpstina = katastarskaOpstina;
                nekretnina.Cena = cena;
                nekretnina.DatumIzgradnje = datumIzgradnje;
                nekretnina.Spratnost = spratnost;
                nekretnina.KratakOpis = kratakOpis;
                nekretnina.DatumOd = datumOd;
                nekretnina.DatumDo = datumDo;
                nekretnina.Tip_Za_Iznajmljivanje = tipZaIznajmljivnje;

                session = DataLayer.GetSession();
                Agent agent = session.Get<Agent>(agentId);
                Klijent vlasnik = session.Get<Klijent>(vlasnikId);
                nekretnina.PripadaAgentu = agent;
                nekretnina.PripadaVlasniku = vlasnik;

                session.Flush();
                session.Save(nekretnina);
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static bool DodajNekretninuZaIznajmljivanjeDuzi(string ulica, string brojUlice, string gradLokacija, string tip,
                            int kvadratura, string brojKatastarskeParcele, string katastarskaOpstina,
                            int cena, DateTime datumIzgradnje, int spratnost, string kratakOpis, int agentId,
                            int vlasnikId, int maksimalniBrojMeseci, string iznajljivac, string tipZaIznajmljivnje = "duzi period")
        {
            ISession session = null;
            try
            {
                NekretninaNaDuziPeriod nekretnina = new NekretninaNaDuziPeriod();
                nekretnina.Ulica = ulica;
                nekretnina.BrojUUlici = brojUlice;
                nekretnina.GradLokacija = gradLokacija;
                nekretnina.Tip = tip;//"iznajmljivanje";
                nekretnina.Kvadratura = kvadratura;
                nekretnina.BrojKatastarskeParcele = brojKatastarskeParcele;
                nekretnina.KatastarskaOpstina = katastarskaOpstina;
                nekretnina.Cena = cena;
                nekretnina.DatumIzgradnje = datumIzgradnje;
                nekretnina.Spratnost = spratnost;
                nekretnina.KratakOpis = kratakOpis;
                
                nekretnina.MaximalniBrojMeseci = maksimalniBrojMeseci;
                nekretnina.Iznajmljivac = iznajljivac;
                nekretnina.Tip_Za_Iznajmljivanje = tipZaIznajmljivnje;

                session = DataLayer.GetSession();
                Agent agent = session.Get<Agent>(agentId);
                Klijent vlasnik = session.Get<Klijent>(vlasnikId);
                nekretnina.PripadaAgentu = agent;
                nekretnina.PripadaVlasniku = vlasnik;

                session.Flush();
                session.Save(nekretnina);
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }

        public static bool ObrisiNekretninu(Nekretnina nekretnina)
        {
            ISession session = null;
            try
            {
                session = DataLayer.GetSession();
                session.Delete(nekretnina);

                session.Flush();
                session.Close();
                return true;
            }
            catch (Exception ex)
            {
                if (session != null)
                    session.Close();
            }
            return false;
        }
    }
}
