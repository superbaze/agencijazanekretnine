﻿using AgencijaZaNekretnine.Mapiranja;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine
{
    public class DataLayer
    {
        private static ISessionFactory factory = null;
        private static object objLock = new object();

        public static ISession GetSession()
        {
            if (factory == null)
            {
                lock(objLock)
                {
                    if (factory == null)
                        factory = CreateSessionFactory();
                }

            }
            return factory.OpenSession();
        }

        private static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var cfg = OracleManagedDataClientConfiguration.
                    Oracle10.ConnectionString(c =>
                    c.Is("Data Source=gislab-oracle.elfak.ni.ac.rs:1521/SBP_PDB;User Id=S15967;Password=S15967"));

                return Fluently.Configure().Database(cfg.ShowSql())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ZaposleniMapiranja>())
                    .BuildSessionFactory();

            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
        }
    }
}
