﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgencijaZaNekretnine.Entiteti;

namespace AgencijaZaNekretnine.Forme
{
    public partial class NekretninaKontrol : UserControl
    {
        #region attributes
        private Nekretnina nekretnina;
        #endregion;
        #region constructors
        public NekretninaKontrol()
        {
            InitializeComponent();
            cbxAgent.DataSource = DTOManager.VratiAgente();
            cbxVlasnik.DataSource = DTOManager.VratiSveKlijente();
        }
        public NekretninaKontrol(Nekretnina nekretnina) : this()
        {
            this.Nekretnina = nekretnina;
        }
        #endregion
        #region properties
        public Nekretnina Nekretnina
        {
            get
            {
                return nekretnina;
            }
            set
            {
                nekretnina = value;
                OsveziKontrole();
            }
        }
        #endregion
        #region methods
        public void OsveziKontrole()
        {
            if (nekretnina != null)
            {
                this.txtGrad.Text = nekretnina.GradLokacija;
                this.txtUlica.Text = nekretnina.Ulica;
                this.txtBrojUUlici.Text = nekretnina.BrojUUlici;
                this.cbxTip.SelectedIndex = this.cbxTip.FindString(nekretnina.Tip);
                this.nudSpratnost.Value = nekretnina.Spratnost;
                this.nudKvadratura.Value = nekretnina.Kvadratura;
                this.nudCena.Value = nekretnina.Cena;
                this.dtpDatumIzgradnje.Value = nekretnina.DatumIzgradnje;
                this.txtKatOpstina.Text = nekretnina.KatastarskaOpstina;
                this.txtBrojKatParcele.Text = nekretnina.BrojKatastarskeParcele;
                this.rchKratakOpis.Text = nekretnina.KratakOpis;

                this.lbxFotografije.Items.Clear();
                this.lbxFotografije.Items.AddRange(nekretnina.Fotografije.ToArray());
            }
        }
        #endregion
    }
}
