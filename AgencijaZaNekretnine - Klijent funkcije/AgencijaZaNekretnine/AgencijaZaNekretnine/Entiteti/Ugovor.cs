﻿
using System;

namespace AgencijaZaNekretnine.Entiteti
{
    public class Ugovor
    {
        public virtual int Broj { get; set; }
        public virtual DateTime DatumPotpisivanja { get; set; }
        public virtual float NaknadaZaPosredovanje { get; set; }
        public virtual float BonusZaAgenta { get; set; }
        public virtual Agent PripadaAgentu { get; set; }
        public virtual Klijent Kupac { get; set; }
        public virtual PravniZastupnik ZastupnikKupca { get; set; }
        public virtual PravniZastupnik ZastupnikVlasnika { get; set; }
    }
}