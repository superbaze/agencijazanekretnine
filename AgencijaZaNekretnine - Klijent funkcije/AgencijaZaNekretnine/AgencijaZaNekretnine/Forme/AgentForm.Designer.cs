﻿namespace AgencijaZaNekretnine.Forme
{
    partial class AgentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnUgovori = new System.Windows.Forms.Button();
            this.btnNekretnine = new System.Windows.Forms.Button();
            this.btnKreirajUgovor = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 62);
            this.dgv.Name = "dgv";
            this.dgv.RowTemplate.Height = 24;
            this.dgv.Size = new System.Drawing.Size(638, 299);
            this.dgv.TabIndex = 0;
            // 
            // btnUgovori
            // 
            this.btnUgovori.Location = new System.Drawing.Point(119, 22);
            this.btnUgovori.Name = "btnUgovori";
            this.btnUgovori.Size = new System.Drawing.Size(101, 34);
            this.btnUgovori.TabIndex = 1;
            this.btnUgovori.Text = "Ugovori";
            this.btnUgovori.UseVisualStyleBackColor = true;
            this.btnUgovori.Click += new System.EventHandler(this.btnUgovori_Click);
            // 
            // btnNekretnine
            // 
            this.btnNekretnine.Location = new System.Drawing.Point(12, 22);
            this.btnNekretnine.Name = "btnNekretnine";
            this.btnNekretnine.Size = new System.Drawing.Size(101, 34);
            this.btnNekretnine.TabIndex = 2;
            this.btnNekretnine.Text = "Nekretnine";
            this.btnNekretnine.UseVisualStyleBackColor = true;
            this.btnNekretnine.Click += new System.EventHandler(this.btnNekretnine_Click);
            // 
            // btnKreirajUgovor
            // 
            this.btnKreirajUgovor.Location = new System.Drawing.Point(12, 367);
            this.btnKreirajUgovor.Name = "btnKreirajUgovor";
            this.btnKreirajUgovor.Size = new System.Drawing.Size(101, 46);
            this.btnKreirajUgovor.TabIndex = 3;
            this.btnKreirajUgovor.Text = "Kreiraj ugovor";
            this.btnKreirajUgovor.UseVisualStyleBackColor = true;
            this.btnKreirajUgovor.Click += new System.EventHandler(this.btnKreirajUgovor_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLogOut.Location = new System.Drawing.Point(575, 12);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(75, 28);
            this.btnLogOut.TabIndex = 4;
            this.btnLogOut.Text = "Log out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            // 
            // AgentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 425);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.btnKreirajUgovor);
            this.Controls.Add(this.btnNekretnine);
            this.Controls.Add(this.btnUgovori);
            this.Controls.Add(this.dgv);
            this.Name = "AgentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AgentForm";
            this.Load += new System.EventHandler(this.AgentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnUgovori;
        private System.Windows.Forms.Button btnNekretnine;
        private System.Windows.Forms.Button btnKreirajUgovor;
        private System.Windows.Forms.Button btnLogOut;
    }
}