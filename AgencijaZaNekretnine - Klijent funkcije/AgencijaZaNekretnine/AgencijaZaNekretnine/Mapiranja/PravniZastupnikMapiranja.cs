﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class PravniZastupnikMapiranja : ClassMap<PravniZastupnik>
    {
        public PravniZastupnikMapiranja()
        {
            Table("PRAVNI_ZASTUPNIK");

            Id(x => x.ID).GeneratedBy.TriggerIdentity();

            Map(x => x.MatBroj, "MAT_BR");
            Map(x => x.Ime, "IME");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.NazivKancelarije, "NAZIV_KANCELARIJE");
            Map(x => x.Ulica, "ULICA");
            Map(x => x.BrojUUlici, "BROJ_U_ULICI");
            Map(x => x.Grad, "GRAD");

            HasMany(x => x.UgovoriVlasnici).KeyColumn("ID_ZASTUPNIK_VLASNIK").LazyLoad().Inverse().Cascade.All();
            HasMany(x => x.UgovoriKlijenti).KeyColumn("ID_ZASTUPNIK_KUPAC").LazyLoad().Inverse().Cascade.All();
        }
    }
}
