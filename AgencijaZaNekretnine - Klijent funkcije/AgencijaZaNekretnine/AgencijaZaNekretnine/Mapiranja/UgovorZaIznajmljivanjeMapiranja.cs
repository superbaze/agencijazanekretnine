﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class UgovorZaIznajmljivanjeMapiranja : SubclassMap<UgovorZaIznajmljivanje>
    {
        public UgovorZaIznajmljivanjeMapiranja()
        {
            Table("UGOVOR_ZA_IZNAJMLJIVANJE");

            KeyColumn("BROJ");

            Map(x => x.DatumOd, "DATUM_OD");
            Map(x => x.DatumDo, "DATUM_DO");
            Map(x => x.MesecnaRenta, "MESECNA_RENTA");

            References(x => x.PripadaNekretnini).Column("ID_IZNAJMLJIVANJE").LazyLoad();

        }
    }
}
