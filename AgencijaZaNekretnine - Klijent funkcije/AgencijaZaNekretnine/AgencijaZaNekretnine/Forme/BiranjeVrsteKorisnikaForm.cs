﻿using AgencijaZaNekretnine.Forme;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine
{
    public partial class BiranjeVrsteKorisnikaForm : Form
    {
        public BiranjeVrsteKorisnikaForm()
        {
            InitializeComponent();
        }

        private void btnKlijent_Click(object sender, EventArgs e)
        {
            (new LoginRegistracijaForm()).Show();
            this.Hide();
        }

        private void btnZaposleni_Click(object sender, EventArgs e)
        {
            this.Hide();
            DialogResult res = (new LoginForm(Korisnik.Agent)).ShowDialog();
            //(new LoginForm()).Show();
            this.Show();
        }
    }
}
