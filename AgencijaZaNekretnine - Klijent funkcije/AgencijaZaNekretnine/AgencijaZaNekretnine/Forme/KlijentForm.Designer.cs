﻿namespace AgencijaZaNekretnine.Forme
{
    partial class KlijentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIme = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.ckbVidiSve = new System.Windows.Forms.CheckBox();
            this.ckbIznajmioSam = new System.Windows.Forms.CheckBox();
            this.ckbVlasnikSam = new System.Windows.Forms.CheckBox();
            this.btnOsvezi = new System.Windows.Forms.Button();
            this.lbxNekretnine = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(12, 9);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(35, 13);
            this.lblIme.TabIndex = 1;
            this.lblIme.Text = "label1";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(147, 9);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(35, 13);
            this.lblPrezime.TabIndex = 2;
            this.lblPrezime.Text = "label2";
            // 
            // ckbVidiSve
            // 
            this.ckbVidiSve.AutoSize = true;
            this.ckbVidiSve.Location = new System.Drawing.Point(345, 41);
            this.ckbVidiSve.Name = "ckbVidiSve";
            this.ckbVidiSve.Size = new System.Drawing.Size(63, 17);
            this.ckbVidiSve.TabIndex = 3;
            this.ckbVidiSve.Text = "Vidi sve";
            this.ckbVidiSve.UseVisualStyleBackColor = true;
            this.ckbVidiSve.CheckedChanged += new System.EventHandler(this.ckbVidiSve_CheckedChanged);
            // 
            // ckbIznajmioSam
            // 
            this.ckbIznajmioSam.AutoSize = true;
            this.ckbIznajmioSam.Enabled = false;
            this.ckbIznajmioSam.Location = new System.Drawing.Point(345, 64);
            this.ckbIznajmioSam.Name = "ckbIznajmioSam";
            this.ckbIznajmioSam.Size = new System.Drawing.Size(86, 17);
            this.ckbIznajmioSam.TabIndex = 4;
            this.ckbIznajmioSam.Text = "Iznajmio sam";
            this.ckbIznajmioSam.UseVisualStyleBackColor = true;
            // 
            // ckbVlasnikSam
            // 
            this.ckbVlasnikSam.AutoSize = true;
            this.ckbVlasnikSam.Enabled = false;
            this.ckbVlasnikSam.Location = new System.Drawing.Point(345, 87);
            this.ckbVlasnikSam.Name = "ckbVlasnikSam";
            this.ckbVlasnikSam.Size = new System.Drawing.Size(82, 17);
            this.ckbVlasnikSam.TabIndex = 5;
            this.ckbVlasnikSam.Text = "Vlasnik sam";
            this.ckbVlasnikSam.UseVisualStyleBackColor = true;
            // 
            // btnOsvezi
            // 
            this.btnOsvezi.Location = new System.Drawing.Point(345, 128);
            this.btnOsvezi.Name = "btnOsvezi";
            this.btnOsvezi.Size = new System.Drawing.Size(75, 23);
            this.btnOsvezi.TabIndex = 6;
            this.btnOsvezi.Text = "Osvezi";
            this.btnOsvezi.UseVisualStyleBackColor = true;
            this.btnOsvezi.Click += new System.EventHandler(this.btnOsvezi_Click);
            // 
            // lbxNekretnine
            // 
            this.lbxNekretnine.FormattingEnabled = true;
            this.lbxNekretnine.Location = new System.Drawing.Point(15, 41);
            this.lbxNekretnine.Name = "lbxNekretnine";
            this.lbxNekretnine.Size = new System.Drawing.Size(324, 446);
            this.lbxNekretnine.TabIndex = 7;
            // 
            // KlijentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 542);
            this.Controls.Add(this.lbxNekretnine);
            this.Controls.Add(this.btnOsvezi);
            this.Controls.Add(this.ckbVlasnikSam);
            this.Controls.Add(this.ckbIznajmioSam);
            this.Controls.Add(this.ckbVidiSve);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.Name = "KlijentForm";
            this.Text = "KlijentForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.CheckBox ckbVidiSve;
        private System.Windows.Forms.CheckBox ckbIznajmioSam;
        private System.Windows.Forms.CheckBox ckbVlasnikSam;
        private System.Windows.Forms.Button btnOsvezi;
        private System.Windows.Forms.ListBox lbxNekretnine;
    }
}