﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class NekretninaZaIznajmljivanjeMapiranja : SubclassMap<NekretninaZaIznajmljivanje>
    {
        public NekretninaZaIznajmljivanjeMapiranja()
        {
            HasMany(x => x.Ugovori).KeyColumn("ID_IZNAJMLJIVANJE").LazyLoad().Inverse().Cascade.All();
        }
    }

    class NekretninaNaKraciPeriodMapiranja : SubclassMap<NekretninaNaKraciPeriod>
    {
        public NekretninaNaKraciPeriodMapiranja()
        {

            Map(x => x.DatumOd, "DATUM_OD");
            Map(x => x.DatumDo, "DATUM_DO");

        }
    }

    class NekretninaNaDuziPeriodMapiranja : SubclassMap<NekretninaNaDuziPeriod>
    {
        public NekretninaNaDuziPeriodMapiranja()
        {


            Map(x => x.MaximalniBrojMeseci, "MAXIMALNI_BROJ_MESECI");
            Map(x => x.Iznajmljivac, "IZNAJMLJIVAC");
        }
    }
}
