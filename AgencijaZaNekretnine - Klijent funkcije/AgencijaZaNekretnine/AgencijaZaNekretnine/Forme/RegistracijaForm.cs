﻿using AgencijaZaNekretnine.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine.Forme
{
    public partial class RegistracijaForm : Form
    {
        public RegistracijaForm()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            new KlijentForm( new KlijentDTO(DTOManager.Register(txtIme.Text, txtPrezime.Text, txtSifra.Text, txtEmail.Text, txtUlica.Text,
                txtBrojUUlici.Text, txtGrad.Text, txtBrojTelefona.Text)));
        }
    }
}
