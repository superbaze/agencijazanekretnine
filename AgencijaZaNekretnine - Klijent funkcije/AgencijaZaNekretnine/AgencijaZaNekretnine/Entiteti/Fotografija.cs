﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class Fotografija
    {
        public virtual FotografijaID id {get; set;}
        public virtual string Sadrzaj { get; set; }

        public Fotografija()
        {
            id = new FotografijaID();
        }
    }
}
