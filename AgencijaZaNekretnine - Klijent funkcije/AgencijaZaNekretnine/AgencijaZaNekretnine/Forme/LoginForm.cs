﻿using AgencijaZaNekretnine.DTOs;
using AgencijaZaNekretnine.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine.Forme
{
    public partial class LoginForm : Form
    {
        private Korisnik State { get; set; } //Za koga se odnosi LogIn 0-klijent 1-agent 
        private LoginForm()
        {
            InitializeComponent();
        }

        public LoginForm(Korisnik korisnik) : this()
        {
            State = korisnik;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (State == Korisnik.Klijent)
                {
                    Klijent k = DTOManager.LogIn(txtEmail.Text, txtSifra.Text);
                    KlijentDTO kdto = new KlijentDTO(k);
                    (new KlijentForm(kdto)).Show();
                    this.Hide();
                }
                if (State == Korisnik.Agent)
                {
                    Agent a = DTOManager.LogInAgent(txtEmail.Text, txtSifra.Text);
                    if(a == null)
                    {
                        MessageBox.Show("Pogresno ime/lozinka", "Los logIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    this.Hide();
                    DialogResult result = (new AgentForm(a)).ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        txtEmail.Text = "";
                        txtSifra.Text = "";
                        this.Show();
                    }
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show("Error");
                this.txtEmail.Text = "";
                this.txtSifra.Text = "";
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            if(State == Korisnik.Agent)
            {
                lblEmail.Text = "Ime";
            }
        }
    }

    public enum Korisnik
    {
        Klijent,
        Agent,
        Administrator
    }
}
