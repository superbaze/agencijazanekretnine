﻿using AgencijaZaNekretnine.DTOs;
using AgencijaZaNekretnine.Entiteti;
using NHibernate;
using NHibernate.Proxy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //   Klijent k = DTOManager.LogIn("dragancedjordjo@gmail.com", "draganrocks");
            List<Klijent> k = DTOManager.VratiSveKlijente();
            List<NekretninaOsnovneInformacije> n = DTOManager.VratiNekretnineIznajmljene(2);
        }

        private void LoadFromDB_Click(object sender, EventArgs e)
        {
            try
            {
                #region Zaposleni
                ISession s = DataLayer.GetSession();
                IList<Zaposleni> zaposleni = s.QueryOver<Zaposleni>().List<Zaposleni>();
                String text = "";
                foreach (Zaposleni z in zaposleni)
                {
                    if (z.GetType() == typeof(Administrator))
                    {
                        Administrator administrator = (Administrator)z;
                        text += "Administrator: " + administrator.Id + "\n";
                    }
                    else
                    {
                        Agent agent = (Agent)z;
                        text += "Agent: " + agent.Ime + "\n";
                    }
                }
                s.Close();
                MessageBox.Show(text);
                #endregion

                #region Klijenti
                s = DataLayer.GetSession();
                text = "";
                IList<Klijent> klijents = s.QueryOver<Klijent>().List<Klijent>();
                foreach (Klijent klijent in klijents)
                {
                    text += "Klijent: " + klijent.Ime + "\n";
                }
                s.Close();
                MessageBox.Show(text);
                #endregion

                #region Ugovori
                s = DataLayer.GetSession();

                IList<Ugovor> ugovori = s.QueryOver<Ugovor>().List<Ugovor>();
                text = "";
                foreach (Ugovor ugovor in ugovori)
                {
                    if (ugovor.GetType() == typeof(UgovorZaIznajmljivanje))
                    {
                        UgovorZaIznajmljivanje izn = (UgovorZaIznajmljivanje)ugovor;
                        text += "Ugovor - Iznajmljivanje: " + izn.Broj + "\n";
                    }
                    else
                    {
                        UgovorZaProdaju prod = (UgovorZaProdaju)ugovor;
                        text += "Ugovor - Prodaja: " + prod.Broj + "\n";
                    }
                }

                MessageBox.Show(text);

                s.Close();
                #endregion

                #region Pravni Zastupnici
                s = DataLayer.GetSession();
                text = "";
                IList<PravniZastupnik> zastupnici = s.QueryOver<PravniZastupnik>().List<PravniZastupnik>();
                foreach (PravniZastupnik zastupnik in zastupnici)
                {
                    text += "Pravni zastupnik: " + zastupnik.Ime + "\n";
                }
                s.Close();
                MessageBox.Show(text);
                #endregion

                #region Nekretnine
                s = DataLayer.GetSession();
                text = "";
                IList<Nekretnina> nekretnine = s.QueryOver<Nekretnina>().List<Nekretnina>();
                foreach (Nekretnina n in nekretnine)
                {
                    if (n.GetType() == typeof(NekretninaZaProdaju))
                    {
                        NekretninaZaProdaju np = (NekretninaZaProdaju)n;
                        text += "Nekretnina Za Prodaju: " + np.Id + "\n";
                    }
                    if (n.GetType() == typeof(NekretninaNaDuziPeriod))
                    {
                        NekretninaNaDuziPeriod np = (NekretninaNaDuziPeriod)n;
                        text += "Nekretnina Za Iznajmljivanje - Duzi: " + np.Id + "\n";
                    }
                    if (n.GetType() == typeof(NekretninaNaKraciPeriod))
                    {
                        NekretninaNaKraciPeriod np = (NekretninaNaKraciPeriod)n;
                        text += "Nekretnina Za Iznajmljivanje - Kraci: " + np.Id + "\n";
                    }
                }
                s.Close();
                MessageBox.Show(text);
                #endregion
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajFotografiju_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IList<Nekretnina> nek = s.QueryOver<Nekretnina>().Where(x => x.Id == 2).List();

                Fotografija f = new Fotografija();
                f.Sadrzaj = "Neki sadrzaj";
                f.id = new FotografijaID();
                f.id.Nekretnina = nek[0];

                s.Save(f);
                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajAgenta_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Agent a = new Agent();
                a.Ime = "Pera";
                a.Prezime = "Simic";
                a.RadniStaz = 5;
                a.Sifra = "cikpogodi";
                s.Save(a);

                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajPravniZastupnik_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravniZastupnik p = new PravniZastupnik();
                p.Ime = "Stojko";
                p.Prezime = "Krivi";
                p.BrojUUlici = "5";
                p.Grad = "Trstenik";
                p.MatBroj = "1234567891111";
                p.NazivKancelarije = "NEsto";
                p.Ulica = "3500grosa";

                s.Save(p);

                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajNekretninaZaProdaju_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NekretninaZaProdaju nek = new NekretninaZaProdaju();
                nek.BrojKatastarskeParcele = "12";
                nek.BrojUUlici = "3";
                nek.Cena = 3500;
                nek.DatumIzgradnje = DateTime.Today;
                IList<Agent> agenti = s.QueryOver<Agent>()
                    .Where(x => x.Id == 2).List();
                nek.PripadaAgentu = agenti[0];
                nek.IdFizickogUgovora = "31";
                nek.KratakOpis = "vrlo kratak";
                nek.PripadaVlasniku = s.QueryOver<Klijent>()
                    .Where(x => x.ID == 2).List()[0];
                nek.Spratnost = 5;
                nek.Ulica = "ulicia";

                UgovorZaProdaju u = new UgovorZaProdaju();
                u.BonusZaAgenta = 3;
                u.Broj = 10;
                u.DatumPotpisivanja = DateTime.Today;
                u.ImeNotara = "ko ce ga znati";
                u.Kupac = s.QueryOver<Klijent>()
                    .Where(x => x.ID == 3).List()[0];
                u.NaknadaZaNotara = 1;
                u.OstvarenaCena = 30;
                u.PripadaAgentu = s.QueryOver<Agent>()
                    .Where(x => x.Id == 2).List()[0];
                u.ZastupnikKupca = s.QueryOver<PravniZastupnik>()
                    .Where(x => x.Ime == "Svetlana").List()[0];
                nek.Ugovor = u;
                u.NaknadaZaPosredovanje = 10;
                u.Prodaja = nek;



                s.Save(nek);
                s.Flush();
                s.Save(u);


                s.Flush();
                s.Close();


                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajNekretninaKraci_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NekretninaNaKraciPeriod n = new NekretninaNaKraciPeriod();
                n.BrojKatastarskeParcele = "1";
                n.BrojUUlici = "2";
                n.Cena = 200;
                n.DatumDo = DateTime.Today;
                n.DatumIzgradnje = DateTime.Today;
                n.DatumOd = DateTime.Today;
                n.GradLokacija = "negotin";
                n.IdFizickogUgovora = "432";
                n.KatastarskaOpstina = "spava mi se";
                n.KratakOpis = "mnogo lepo";
                n.Kvadratura = 20;
                n.PripadaAgentu = s.QueryOver<Agent>()
                    .Where(x => x.Id == 2).List()[0];
                n.PripadaVlasniku = s.QueryOver<Klijent>()
                    .Where(x => x.ID == 2).List()[0];
                n.Spratnost = 3;
                n.Ulica = "ulicia";

                UgovorZaIznajmljivanje u = new UgovorZaIznajmljivanje();
                u.BonusZaAgenta = 3;
                u.DatumPotpisivanja = DateTime.Today;
                u.Kupac = s.QueryOver<Klijent>()
                    .Where(x => x.ID == 3).List()[0];
                u.PripadaAgentu = s.QueryOver<Agent>()
                    .Where(x => x.Id == 2).List()[0];
                u.ZastupnikVlasnika = s.QueryOver<PravniZastupnik>()
                    .Where(x => x.Ime == "Svetlana").List()[0];
                u.NaknadaZaPosredovanje = 10;
                u.DatumDo = DateTime.Today;
                u.DatumOd = DateTime.Today;
                u.DatumPotpisivanja = DateTime.Today;

                u.PripadaNekretnini = n;
                u.MesecnaRenta = 50;

                s.Save(n);
                s.Save(u);

                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajNekretninaDuzi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NekretninaNaDuziPeriod n = new NekretninaNaDuziPeriod();
                n.BrojKatastarskeParcele = "1";
                n.BrojUUlici = "2";
                n.Cena = 200;
                n.DatumIzgradnje = DateTime.Today;
                n.GradLokacija = "negotin";
                n.IdFizickogUgovora = "432";
                n.KatastarskaOpstina = "spava mi se";
                n.KratakOpis = "mnogo lepo";
                n.Kvadratura = 20;
                n.PripadaAgentu = s.QueryOver<Agent>()
                    .Where(x => x.Id == 2).List()[0];
                n.PripadaVlasniku = s.QueryOver<Klijent>()
                    .Where(x => x.ID == 2).List()[0];
                n.Spratnost = 3;
                n.Ulica = "ulicia";
                n.MaximalniBrojMeseci = 12;


                UgovorZaIznajmljivanje u = new UgovorZaIznajmljivanje();
                u.BonusZaAgenta = 3;
                u.DatumPotpisivanja = DateTime.Today;
                u.Kupac = s.QueryOver<Klijent>()
                    .Where(x => x.ID == 4).List()[0];
                u.PripadaAgentu = s.QueryOver<Agent>()
                    .Where(x => x.Id == 3).List()[0];
                u.ZastupnikVlasnika = s.QueryOver<PravniZastupnik>()
                    .Where(x => x.Ime == "Svetlana").List()[0];
                u.NaknadaZaPosredovanje = 20;
                u.DatumDo = DateTime.Today;
                u.DatumOd = DateTime.Today;
                u.DatumPotpisivanja = DateTime.Today;
                u.PripadaNekretnini = n;
                u.MesecnaRenta = 40;

                s.Save(n);
                s.Save(u);

                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajMail_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Agent a = s.QueryOver<Agent>()
                    .Where(x => x.Id == 3).List()[0];
                Email mail = new Email();
                mail.IdAgenta = a;
                mail.E_mail = "nesot@jos malo";

                s.Save(mail);
                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajTelefon_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Agent a = s.QueryOver<Agent>()
                    .Where(x => x.Id == 3).List()[0];
                BrojTelefona telefon = new BrojTelefona();
                telefon.BrTelefona = "06060606060";
                telefon.IdAgenta = a;

                s.Save(telefon);
                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DodajKlijenta_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Klijent k = new Klijent();
                k.BrojTelefona = "0633243444";
                k.BrojUUlici = "3";
                k.E_mail = "nekimail@mail.com";
                k.Grad = "Gradiste";
                k.Ime = "Milos";
                k.Prezime = "Zecevic";
                k.JeKupac = 1;
                k.JeVlasnik = 1;
                k.Sifra = "cik_pogodi";
                k.Ulica = "velikih_ljudi";

                s.Save(k);

                s.Flush();
                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ObrisiKreirano_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IList<Ugovor> ugovori = s.QueryOver<Ugovor>()
                    .Where(x => x.NaknadaZaPosredovanje == 10 && x.BonusZaAgenta == 3).List();
                foreach (Ugovor u in ugovori)
                {
                    s.Delete(u);
                }
                s.Flush();

                IList<Nekretnina> nekretnineZaBrisanje = s.QueryOver<Nekretnina>()
                    .Where(x => x.Ulica == "ulicia").List();
                foreach (Nekretnina n in nekretnineZaBrisanje)
                {
                    s.Delete(n);
                }
                s.Flush();

                IList<Zaposleni> zaposleni = s.QueryOver<Zaposleni>()
                    .Where(x => x.Sifra == "cikpogodi").List();
                foreach (Zaposleni z in zaposleni)
                {
                    s.Delete(z);
                }
                s.Flush();

                IList<Klijent> klijenti = s.QueryOver<Klijent>()
                    .Where(x => x.E_mail == "nekimail@mail.com").List();
                foreach (Klijent k in klijenti)
                {
                    s.Delete(k);
                }
                s.Flush();

                IList<BrojTelefona> telefoni = s.QueryOver<BrojTelefona>()
                    .Where(x => x.BrTelefona == "06060606060").List();
                foreach (BrojTelefona t in telefoni)
                {
                    s.Delete(t);
                }
                s.Flush();

                IList<Email> mailovi = s.QueryOver<Email>()
                    .Where(x => x.E_mail == "nesot@jos malo").List();
                foreach (Email m in mailovi)
                {
                    s.Delete(m);
                }
                s.Flush();

                IList<Fotografija> fotografije = s.QueryOver<Fotografija>()
                    .Where(x => x.Sadrzaj == "Neki sadrzaj").List();
                foreach (Fotografija f in fotografije)
                {
                    s.Delete(f);
                }
                s.Flush();

                IList<PravniZastupnik> zastupnici = s.QueryOver<PravniZastupnik>()
                    .Where(x => x.Ime == "Stojko").List();
                foreach (PravniZastupnik zas in zastupnici)
                {
                    s.Delete(zas);
                }
                s.Flush();

                s.Close();

                MessageBox.Show("Akcija uspesno izvrsena");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDodajUgovorIznajmljivanje_Click(object sender, EventArgs e)
        {
            DTOManager.KreirajUgovorZaIznajmljivanje(DateTime.Today, 10, 10, 8, 1, 3, DateTime.Today,
                DateTime.Today, 10, 1, 2);
        }
    }
}
