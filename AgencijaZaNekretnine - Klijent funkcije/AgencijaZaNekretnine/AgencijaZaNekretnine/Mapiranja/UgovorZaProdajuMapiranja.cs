﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class UgovorZaProdajuMapiranja : SubclassMap<UgovorZaProdaju>
    {
        public UgovorZaProdajuMapiranja()
        {
            Table("UGOVOR_ZA_PRODAJU");

            KeyColumn("BROJ");

            Map(x => x.OstvarenaCena, "OSTVARENA_CENA");
            Map(x => x.NaknadaZaNotara, "NAKNADA_ZA_NOTARA");
            Map(x => x.ImeNotara, "IME_NOTARA");

           
            References(x => x.Prodaja).Column("ID_PRODAJA").LazyLoad(); 

        }
    }
}
