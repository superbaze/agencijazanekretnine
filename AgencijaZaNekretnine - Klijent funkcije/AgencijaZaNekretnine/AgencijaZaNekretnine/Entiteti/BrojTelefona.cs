﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class BrojTelefona
    {
        public virtual Agent IdAgenta { get; set; }
        public virtual string BrTelefona { get; set; }


        public override bool Equals(object obj)
        {

            if (object.ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(BrojTelefona))
                return false;

            BrojTelefona recvObj = (BrojTelefona)obj;

            if ((IdAgenta.Id == recvObj.IdAgenta.Id) && (BrTelefona == recvObj.BrTelefona))
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

       
    }
}
