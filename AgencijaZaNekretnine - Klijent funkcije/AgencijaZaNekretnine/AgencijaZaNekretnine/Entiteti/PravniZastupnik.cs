﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class PravniZastupnik
    {
        public virtual int ID { get; set; }
        public virtual string MatBroj { get; set; }
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual string NazivKancelarije { get; set; }
        public virtual string Ulica { get; set; }
        public virtual string BrojUUlici { get; set; }
        public virtual string Grad { get; set; }

        public virtual IList<Ugovor> UgovoriVlasnici { get; set; }
        public virtual IList<Ugovor> UgovoriKlijenti { get; set; }

        public PravniZastupnik()
        {
            UgovoriKlijenti = new List<Ugovor>();
            UgovoriVlasnici = new List<Ugovor>();
        }

        public override string ToString()
        {
            return Ime +" " + Prezime +" "+ "\"" + NazivKancelarije + "\"";
        }
    }
}
