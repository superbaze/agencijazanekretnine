﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class ZaposleniMapiranja: ClassMap<Zaposleni>
    {
        public ZaposleniMapiranja()
        {
            Table("ZAPOSLENI");

            DiscriminateSubClassesOnColumn("TIP_ZAPOSLENOG");

            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            Map(x => x.Sifra, "SIFRA");
        }

    }

    class AdministratorMapiranja: SubclassMap<Administrator>
    {
        public AdministratorMapiranja()
        {
            DiscriminatorValue("administrator");
        }
    }

    class AgentMapiranja: SubclassMap<Agent>
    {
        public AgentMapiranja()
        {
        
            DiscriminatorValue("agent");

            Map(x => x.Ime, "IME");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.RadniStaz, "RADNI_STAZ");

            HasMany(x => x.BrojeviTelefona).KeyColumn("ID_AGENTA").LazyLoad().Cascade.All().Inverse();
            HasMany(x => x.Email).KeyColumn("ID_AGENTA").LazyLoad().Cascade.All().Inverse();
            HasMany(x => x.Nekretnine).KeyColumn("ID_AGENTA").LazyLoad().Cascade.All().Inverse();
            HasMany(x => x.Ugovori).KeyColumn("ID_AGENTA").LazyLoad().Cascade.All().Inverse();
        }
    }
}
