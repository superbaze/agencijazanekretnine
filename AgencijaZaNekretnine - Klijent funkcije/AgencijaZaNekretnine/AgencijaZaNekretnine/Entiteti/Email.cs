﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class Email
    {
        public virtual Agent IdAgenta { get; set; }
        public virtual string E_mail { get; set; }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(Email))
                return false;

            Email recvObj = (Email)obj;

            if ((IdAgenta.Id == recvObj.IdAgenta.Id) && (E_mail == recvObj.E_mail))
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
