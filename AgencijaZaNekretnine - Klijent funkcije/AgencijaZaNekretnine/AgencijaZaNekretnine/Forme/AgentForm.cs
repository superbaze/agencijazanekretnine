﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgencijaZaNekretnine.Entiteti;
using AgencijaZaNekretnine.DTOs;

namespace AgencijaZaNekretnine.Forme
{
    public partial class AgentForm : Form
    {
        Agent agentKorisnik;
        bool DSjeNiliU;

        #region Constructors

        private AgentForm()
        {
            //for testing only:
            //List<Agent> list = DTOManager.VratiAgente();
            //list.ForEach(x => { if (x.Id == 3) agentKorisnik = x; });
            InitializeComponent();
        }

        public AgentForm(Agent agent) : this()
        {
            agentKorisnik = agent;
        }

        #endregion

        #region EventHandlers

        private void btnNekretnine_Click(object sender, EventArgs e)
        {
            List<DTOs.NekretninaOsnovneInformacije> lista
                = DTOManager.VratiNekretnineAgenta(agentKorisnik.Id);
            dgv.DataSource = lista;
            //DgvResize();
            DSjeNiliU = true;
        }

        private void btnUgovori_Click(object sender, EventArgs e)
        {
            List<UgovorDTO> lista = DTOManager.VratiUgovoreAgenta(agentKorisnik.Id);
            dgv.DataSource = lista;
            //DgvResize();
            DSjeNiliU = false;
        }

        private void AgentForm_Load(object sender, EventArgs e)
        {
        }

        private void btnKreirajUgovor_Click(object sender, EventArgs e)
        {
            KreiranjeUgovoraForm frm = new KreiranjeUgovoraForm(agentKorisnik.Id);
            DialogResult result = frm.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (frm.TipUgovora == Tip.Iznajmljivanje)
                {
                    DTOManager.KreirajUgovorZaIznajmljivanje(
                        frm.DatumPotpisivanja,
                        frm.NadoknadaPosredovanje,
                        frm.BonusAgent,
                        agentKorisnik.Id,
                        frm.KupacID,
                        frm.NekretninaID,
                        frm.DatumDo,
                        frm.DatumOd,
                        frm.MesecnaRata,
                        frm.PravniZastupnikProdavac,
                        frm.PravniZastupnikKupac);
                }
                else
                {
                    DTOManager.KreirajUgovorZaProdaju(
                        frm.DatumPotpisivanja,
                        frm.NadoknadaPosredovanje,
                        frm.BonusAgent,
                        agentKorisnik.Id,
                        frm.KupacID,
                        frm.OstvarenaCena,
                        frm.NaknadaZaNotara,
                        frm.ImeNotara,
                        frm.NekretninaID,
                        frm.PravniZastupnikProdavac,
                        frm.PravniZastupnikKupac);
                }
            }
        }

        #endregion

        #region Methods

        private void DgvResize()
        {
            int size = 0;
            foreach (DataGridViewColumn c in dgv.Columns)
            {
                size += c.Width;
            }
            dgv.Width = size + dgv.RowHeadersWidth + 2;
        }

        #endregion

        
    }
}
