﻿namespace AgencijaZaNekretnine.Forme
{
    partial class KreiranjeUgovoraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxTip = new System.Windows.Forms.ComboBox();
            this.lblTip = new System.Windows.Forms.Label();
            this.lblDatumPotpisivanja = new System.Windows.Forms.Label();
            this.lblDatumPotpisivanjaA = new System.Windows.Forms.Label();
            this.lblNadoknada = new System.Windows.Forms.Label();
            this.lblBonus = new System.Windows.Forms.Label();
            this.lblKupac = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxBezZastupnika = new System.Windows.Forms.CheckBox();
            this.cbxPZProdavac = new System.Windows.Forms.ComboBox();
            this.cbxPZKupac = new System.Windows.Forms.ComboBox();
            this.lblPZProdavca = new System.Windows.Forms.Label();
            this.lblPZKupac = new System.Windows.Forms.Label();
            this.gbxIznajmljivanje = new System.Windows.Forms.GroupBox();
            this.tbxMesecnaRata = new System.Windows.Forms.TextBox();
            this.lblMesecnaRata = new System.Windows.Forms.Label();
            this.lblDatumDo = new System.Windows.Forms.Label();
            this.dtpDo = new System.Windows.Forms.DateTimePicker();
            this.dtpOd = new System.Windows.Forms.DateTimePicker();
            this.lblDatumOd = new System.Windows.Forms.Label();
            this.gbxProdaja = new System.Windows.Forms.GroupBox();
            this.tbxNaknadaNotar = new System.Windows.Forms.TextBox();
            this.tbxImeNotara = new System.Windows.Forms.TextBox();
            this.tbxOstvarenaCena = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNotarIme = new System.Windows.Forms.Label();
            this.lblCena = new System.Windows.Forms.Label();
            this.cbxKupac = new System.Windows.Forms.ComboBox();
            this.tbxBonusAgent = new System.Windows.Forms.TextBox();
            this.tbxNadoknadaPos = new System.Windows.Forms.TextBox();
            this.listboxNekretnine = new System.Windows.Forms.ListBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.gbxIznajmljivanje.SuspendLayout();
            this.gbxProdaja.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxTip
            // 
            this.cbxTip.FormattingEnabled = true;
            this.cbxTip.Location = new System.Drawing.Point(54, 6);
            this.cbxTip.Name = "cbxTip";
            this.cbxTip.Size = new System.Drawing.Size(121, 24);
            this.cbxTip.TabIndex = 0;
            this.cbxTip.SelectedIndexChanged += new System.EventHandler(this.cbxTip_SelectedIndexChanged);
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(12, 9);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(36, 17);
            this.lblTip.TabIndex = 1;
            this.lblTip.Text = "Tip: ";
            // 
            // lblDatumPotpisivanja
            // 
            this.lblDatumPotpisivanja.AutoSize = true;
            this.lblDatumPotpisivanja.Location = new System.Drawing.Point(214, 9);
            this.lblDatumPotpisivanja.Name = "lblDatumPotpisivanja";
            this.lblDatumPotpisivanja.Size = new System.Drawing.Size(132, 17);
            this.lblDatumPotpisivanja.TabIndex = 2;
            this.lblDatumPotpisivanja.Text = "Datum potpisivanja:";
            // 
            // lblDatumPotpisivanjaA
            // 
            this.lblDatumPotpisivanjaA.AutoSize = true;
            this.lblDatumPotpisivanjaA.Location = new System.Drawing.Point(392, 9);
            this.lblDatumPotpisivanjaA.Name = "lblDatumPotpisivanjaA";
            this.lblDatumPotpisivanjaA.Size = new System.Drawing.Size(46, 17);
            this.lblDatumPotpisivanjaA.TabIndex = 3;
            this.lblDatumPotpisivanjaA.Text = "label1";
            // 
            // lblNadoknada
            // 
            this.lblNadoknada.AutoSize = true;
            this.lblNadoknada.Location = new System.Drawing.Point(12, 80);
            this.lblNadoknada.Name = "lblNadoknada";
            this.lblNadoknada.Size = new System.Drawing.Size(194, 17);
            this.lblNadoknada.TabIndex = 4;
            this.lblNadoknada.Text = "Nadoknada za posredovanje:";
            // 
            // lblBonus
            // 
            this.lblBonus.AutoSize = true;
            this.lblBonus.Location = new System.Drawing.Point(12, 116);
            this.lblBonus.Name = "lblBonus";
            this.lblBonus.Size = new System.Drawing.Size(119, 17);
            this.lblBonus.TabIndex = 5;
            this.lblBonus.Text = "Bonus za agenta:";
            // 
            // lblKupac
            // 
            this.lblKupac.AutoSize = true;
            this.lblKupac.Location = new System.Drawing.Point(12, 155);
            this.lblKupac.Name = "lblKupac";
            this.lblKupac.Size = new System.Drawing.Size(52, 17);
            this.lblKupac.TabIndex = 6;
            this.lblKupac.Text = "Kupac:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxBezZastupnika);
            this.groupBox1.Controls.Add(this.cbxPZProdavac);
            this.groupBox1.Controls.Add(this.cbxPZKupac);
            this.groupBox1.Controls.Add(this.lblPZProdavca);
            this.groupBox1.Controls.Add(this.lblPZKupac);
            this.groupBox1.Location = new System.Drawing.Point(15, 199);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 128);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pravni zastupnici";
            // 
            // cbxBezZastupnika
            // 
            this.cbxBezZastupnika.AutoSize = true;
            this.cbxBezZastupnika.Location = new System.Drawing.Point(183, 101);
            this.cbxBezZastupnika.Name = "cbxBezZastupnika";
            this.cbxBezZastupnika.Size = new System.Drawing.Size(126, 21);
            this.cbxBezZastupnika.TabIndex = 4;
            this.cbxBezZastupnika.Text = "Bez zastupnika";
            this.cbxBezZastupnika.UseVisualStyleBackColor = true;
            this.cbxBezZastupnika.CheckedChanged += new System.EventHandler(this.cbxBezZastupnika_CheckedChanged);
            // 
            // cbxPZProdavac
            // 
            this.cbxPZProdavac.FormattingEnabled = true;
            this.cbxPZProdavac.Location = new System.Drawing.Point(88, 71);
            this.cbxPZProdavac.Name = "cbxPZProdavac";
            this.cbxPZProdavac.Size = new System.Drawing.Size(365, 24);
            this.cbxPZProdavac.TabIndex = 3;
            // 
            // cbxPZKupac
            // 
            this.cbxPZKupac.FormattingEnabled = true;
            this.cbxPZKupac.Location = new System.Drawing.Point(88, 32);
            this.cbxPZKupac.Name = "cbxPZKupac";
            this.cbxPZKupac.Size = new System.Drawing.Size(365, 24);
            this.cbxPZKupac.TabIndex = 2;
            // 
            // lblPZProdavca
            // 
            this.lblPZProdavca.AutoSize = true;
            this.lblPZProdavca.Location = new System.Drawing.Point(6, 74);
            this.lblPZProdavca.Name = "lblPZProdavca";
            this.lblPZProdavca.Size = new System.Drawing.Size(72, 17);
            this.lblPZProdavca.TabIndex = 1;
            this.lblPZProdavca.Text = "Prodavac:";
            // 
            // lblPZKupac
            // 
            this.lblPZKupac.AutoSize = true;
            this.lblPZKupac.Location = new System.Drawing.Point(6, 35);
            this.lblPZKupac.Name = "lblPZKupac";
            this.lblPZKupac.Size = new System.Drawing.Size(52, 17);
            this.lblPZKupac.TabIndex = 0;
            this.lblPZKupac.Text = "Kupac:";
            // 
            // gbxIznajmljivanje
            // 
            this.gbxIznajmljivanje.Controls.Add(this.tbxMesecnaRata);
            this.gbxIznajmljivanje.Controls.Add(this.lblMesecnaRata);
            this.gbxIznajmljivanje.Controls.Add(this.lblDatumDo);
            this.gbxIznajmljivanje.Controls.Add(this.dtpDo);
            this.gbxIznajmljivanje.Controls.Add(this.dtpOd);
            this.gbxIznajmljivanje.Controls.Add(this.lblDatumOd);
            this.gbxIznajmljivanje.Controls.Add(this.gbxProdaja);
            this.gbxIznajmljivanje.Location = new System.Drawing.Point(15, 333);
            this.gbxIznajmljivanje.Name = "gbxIznajmljivanje";
            this.gbxIznajmljivanje.Size = new System.Drawing.Size(459, 162);
            this.gbxIznajmljivanje.TabIndex = 8;
            this.gbxIznajmljivanje.TabStop = false;
            this.gbxIznajmljivanje.Text = "Iznajmljivanje";
            this.gbxIznajmljivanje.Visible = false;
            // 
            // tbxMesecnaRata
            // 
            this.tbxMesecnaRata.Location = new System.Drawing.Point(233, 48);
            this.tbxMesecnaRata.Name = "tbxMesecnaRata";
            this.tbxMesecnaRata.Size = new System.Drawing.Size(220, 22);
            this.tbxMesecnaRata.TabIndex = 5;
            this.tbxMesecnaRata.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxMesecnaRata_KeyPress);
            // 
            // lblMesecnaRata
            // 
            this.lblMesecnaRata.AutoSize = true;
            this.lblMesecnaRata.Location = new System.Drawing.Point(230, 28);
            this.lblMesecnaRata.Name = "lblMesecnaRata";
            this.lblMesecnaRata.Size = new System.Drawing.Size(98, 17);
            this.lblMesecnaRata.TabIndex = 4;
            this.lblMesecnaRata.Text = "Mesecna rata:";
            // 
            // lblDatumDo
            // 
            this.lblDatumDo.AutoSize = true;
            this.lblDatumDo.Location = new System.Drawing.Point(6, 86);
            this.lblDatumDo.Name = "lblDatumDo";
            this.lblDatumDo.Size = new System.Drawing.Size(73, 17);
            this.lblDatumDo.TabIndex = 3;
            this.lblDatumDo.Text = "Datum do:";
            // 
            // dtpDo
            // 
            this.dtpDo.Location = new System.Drawing.Point(9, 106);
            this.dtpDo.Name = "dtpDo";
            this.dtpDo.Size = new System.Drawing.Size(200, 22);
            this.dtpDo.TabIndex = 2;
            // 
            // dtpOd
            // 
            this.dtpOd.Location = new System.Drawing.Point(9, 48);
            this.dtpOd.Name = "dtpOd";
            this.dtpOd.Size = new System.Drawing.Size(200, 22);
            this.dtpOd.TabIndex = 1;
            // 
            // lblDatumOd
            // 
            this.lblDatumOd.AutoSize = true;
            this.lblDatumOd.Location = new System.Drawing.Point(6, 28);
            this.lblDatumOd.Name = "lblDatumOd";
            this.lblDatumOd.Size = new System.Drawing.Size(73, 17);
            this.lblDatumOd.TabIndex = 0;
            this.lblDatumOd.Text = "Datum od:";
            // 
            // gbxProdaja
            // 
            this.gbxProdaja.Controls.Add(this.tbxNaknadaNotar);
            this.gbxProdaja.Controls.Add(this.tbxImeNotara);
            this.gbxProdaja.Controls.Add(this.tbxOstvarenaCena);
            this.gbxProdaja.Controls.Add(this.label1);
            this.gbxProdaja.Controls.Add(this.lblNotarIme);
            this.gbxProdaja.Controls.Add(this.lblCena);
            this.gbxProdaja.Location = new System.Drawing.Point(0, 0);
            this.gbxProdaja.Name = "gbxProdaja";
            this.gbxProdaja.Size = new System.Drawing.Size(459, 162);
            this.gbxProdaja.TabIndex = 9;
            this.gbxProdaja.TabStop = false;
            this.gbxProdaja.Text = "Prodaja";
            // 
            // tbxNaknadaNotar
            // 
            this.tbxNaknadaNotar.Location = new System.Drawing.Point(355, 103);
            this.tbxNaknadaNotar.Name = "tbxNaknadaNotar";
            this.tbxNaknadaNotar.Size = new System.Drawing.Size(100, 22);
            this.tbxNaknadaNotar.TabIndex = 5;
            this.tbxNaknadaNotar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxNaknadaNotar_KeyPress);
            // 
            // tbxImeNotara
            // 
            this.tbxImeNotara.Location = new System.Drawing.Point(94, 103);
            this.tbxImeNotara.Name = "tbxImeNotara";
            this.tbxImeNotara.Size = new System.Drawing.Size(180, 22);
            this.tbxImeNotara.TabIndex = 4;
            // 
            // tbxOstvarenaCena
            // 
            this.tbxOstvarenaCena.Location = new System.Drawing.Point(203, 48);
            this.tbxOstvarenaCena.Name = "tbxOstvarenaCena";
            this.tbxOstvarenaCena.Size = new System.Drawing.Size(227, 22);
            this.tbxOstvarenaCena.TabIndex = 3;
            this.tbxOstvarenaCena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxOstvarenaCena_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(280, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Naknada:";
            // 
            // lblNotarIme
            // 
            this.lblNotarIme.AutoSize = true;
            this.lblNotarIme.Location = new System.Drawing.Point(9, 106);
            this.lblNotarIme.Name = "lblNotarIme";
            this.lblNotarIme.Size = new System.Drawing.Size(79, 17);
            this.lblNotarIme.TabIndex = 1;
            this.lblNotarIme.Text = "Ime notara:";
            // 
            // lblCena
            // 
            this.lblCena.AutoSize = true;
            this.lblCena.Location = new System.Drawing.Point(30, 48);
            this.lblCena.Name = "lblCena";
            this.lblCena.Size = new System.Drawing.Size(113, 17);
            this.lblCena.TabIndex = 0;
            this.lblCena.Text = "Ostvarena cena:";
            // 
            // cbxKupac
            // 
            this.cbxKupac.FormattingEnabled = true;
            this.cbxKupac.Location = new System.Drawing.Point(101, 152);
            this.cbxKupac.Name = "cbxKupac";
            this.cbxKupac.Size = new System.Drawing.Size(337, 24);
            this.cbxKupac.TabIndex = 10;
            // 
            // tbxBonusAgent
            // 
            this.tbxBonusAgent.Location = new System.Drawing.Point(173, 113);
            this.tbxBonusAgent.Name = "tbxBonusAgent";
            this.tbxBonusAgent.Size = new System.Drawing.Size(265, 22);
            this.tbxBonusAgent.TabIndex = 11;
            this.tbxBonusAgent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxBonusAgent_KeyPress);
            // 
            // tbxNadoknadaPos
            // 
            this.tbxNadoknadaPos.Location = new System.Drawing.Point(233, 77);
            this.tbxNadoknadaPos.Name = "tbxNadoknadaPos";
            this.tbxNadoknadaPos.Size = new System.Drawing.Size(205, 22);
            this.tbxNadoknadaPos.TabIndex = 12;
            this.tbxNadoknadaPos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxNadoknadaPos_KeyPress);
            // 
            // listboxNekretnine
            // 
            this.listboxNekretnine.FormattingEnabled = true;
            this.listboxNekretnine.ItemHeight = 16;
            this.listboxNekretnine.Location = new System.Drawing.Point(490, 9);
            this.listboxNekretnine.Name = "listboxNekretnine";
            this.listboxNekretnine.Size = new System.Drawing.Size(382, 484);
            this.listboxNekretnine.TabIndex = 13;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(783, 501);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 34);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(688, 501);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(89, 34);
            this.btnOK.TabIndex = 15;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // KreiranjeUgovoraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 547);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.listboxNekretnine);
            this.Controls.Add(this.tbxNadoknadaPos);
            this.Controls.Add(this.tbxBonusAgent);
            this.Controls.Add(this.cbxKupac);
            this.Controls.Add(this.gbxIznajmljivanje);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblKupac);
            this.Controls.Add(this.lblBonus);
            this.Controls.Add(this.lblNadoknada);
            this.Controls.Add(this.lblDatumPotpisivanjaA);
            this.Controls.Add(this.lblDatumPotpisivanja);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.cbxTip);
            this.Name = "KreiranjeUgovoraForm";
            this.Text = "KreiranjeUgovoraForm";
            this.Load += new System.EventHandler(this.KreiranjeUgovoraForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxIznajmljivanje.ResumeLayout(false);
            this.gbxIznajmljivanje.PerformLayout();
            this.gbxProdaja.ResumeLayout(false);
            this.gbxProdaja.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxTip;
        private System.Windows.Forms.Label lblTip;
        private System.Windows.Forms.Label lblDatumPotpisivanja;
        private System.Windows.Forms.Label lblDatumPotpisivanjaA;
        private System.Windows.Forms.Label lblNadoknada;
        private System.Windows.Forms.Label lblBonus;
        private System.Windows.Forms.Label lblKupac;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPZKupac;
        private System.Windows.Forms.Label lblPZProdavca;
        private System.Windows.Forms.GroupBox gbxIznajmljivanje;
        private System.Windows.Forms.Label lblDatumDo;
        private System.Windows.Forms.DateTimePicker dtpDo;
        private System.Windows.Forms.DateTimePicker dtpOd;
        private System.Windows.Forms.Label lblDatumOd;
        private System.Windows.Forms.Label lblMesecnaRata;
        private System.Windows.Forms.GroupBox gbxProdaja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNotarIme;
        private System.Windows.Forms.Label lblCena;
        private System.Windows.Forms.ComboBox cbxPZProdavac;
        private System.Windows.Forms.ComboBox cbxPZKupac;
        private System.Windows.Forms.TextBox tbxMesecnaRata;
        private System.Windows.Forms.TextBox tbxNaknadaNotar;
        private System.Windows.Forms.TextBox tbxImeNotara;
        private System.Windows.Forms.TextBox tbxOstvarenaCena;
        private System.Windows.Forms.ComboBox cbxKupac;
        private System.Windows.Forms.TextBox tbxBonusAgent;
        private System.Windows.Forms.TextBox tbxNadoknadaPos;
        private System.Windows.Forms.CheckBox cbxBezZastupnika;
        private System.Windows.Forms.ListBox listboxNekretnine;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}