﻿using AgencijaZaNekretnine.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine.Forme
{
    public partial class DodaIzmenijNekretninuForm : Form
    {
        private bool add;
        public DodaIzmenijNekretninuForm()
        {
            InitializeComponent();
            cbxTip.SelectedIndex = 0;
            cbxVrstaIznajmljivanja.SelectedIndex = 0;
            SwitchTip();
            add = true;
        }

        public DodaIzmenijNekretninuForm(Nekretnina n) : this()
        {
            add = false;
        }


        public void SwitchTip()
        {
            cbxVrstaIznajmljivanja.Hide();
            txtIznajmljivac.Hide();
            nudMaxMeseci.Hide();
            dtpDo.Hide();
            dtpOd.Hide();
            lblDo.Hide();
            lblOd.Hide();
            lblIznajmljivac.Hide();
            lblMaxMeseci.Hide();
            txtIznajmljivac.Hide();
            nudMaxMeseci.Hide();

            if (cbxTip.SelectedIndex == 0)
            {
                cbxVrstaIznajmljivanja.Show();
                SwitchVrstaIznajmljivanja();
            }
            else
            {

            }
        }
        public void SwitchVrstaIznajmljivanja()
        {
            txtIznajmljivac.Hide();
            nudMaxMeseci.Hide();
            dtpDo.Hide();
            dtpOd.Hide();
            lblDo.Hide();
            lblOd.Hide();
            lblIznajmljivac.Hide();
            lblMaxMeseci.Hide();
            txtIznajmljivac.Hide();
            nudMaxMeseci.Hide();
            if (cbxVrstaIznajmljivanja.SelectedIndex == 0)
            {
                dtpDo.Show();
                dtpOd.Show();
                lblDo.Show();
                lblOd.Show();
            }
            else
            {
                lblIznajmljivac.Show();
                lblMaxMeseci.Show();
                txtIznajmljivac.Show();
                nudMaxMeseci.Show();
            }
        }

        private void cbxTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            SwitchTip();
        }

        private void cbxVrstaIznajmljivanja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SwitchVrstaIznajmljivanja();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (add)
            {
                if (cbxTip.SelectedIndex == 0)
                {
                    if (cbxVrstaIznajmljivanja.SelectedIndex == 0)
                    {
                        DTOManager.DodajNekretninuZaIznajmljivanjeKraci(nekretninaKontrol.txtUlica.Text, nekretninaKontrol.txtBrojUUlici.Text,
                            nekretninaKontrol.txtGrad.Text, nekretninaKontrol.cbxTip.SelectedItem.ToString(),
                            (int)nekretninaKontrol.nudKvadratura.Value, nekretninaKontrol.txtBrojKatParcele.Text,
                            nekretninaKontrol.txtKatOpstina.Text, (int)nekretninaKontrol.nudCena.Value,
                            nekretninaKontrol.dtpDatumIzgradnje.Value, (int)nekretninaKontrol.nudSpratnost.Value,
                            nekretninaKontrol.rchKratakOpis.Text,
                            ((Agent)nekretninaKontrol.cbxAgent.SelectedItem).Id,
                            ((Klijent)nekretninaKontrol.cbxVlasnik.SelectedItem).ID,
                            dtpOd.Value, dtpDo.Value);
                    }
                    else
                    {
                        DTOManager.DodajNekretninuZaIznajmljivanjeDuzi(nekretninaKontrol.txtUlica.Text, nekretninaKontrol.txtBrojUUlici.Text,
                            nekretninaKontrol.txtGrad.Text, nekretninaKontrol.cbxTip.SelectedItem.ToString(),
                            (int)nekretninaKontrol.nudKvadratura.Value, nekretninaKontrol.txtBrojKatParcele.Text,
                            nekretninaKontrol.txtKatOpstina.Text, (int)nekretninaKontrol.nudCena.Value,
                            nekretninaKontrol.dtpDatumIzgradnje.Value, (int)nekretninaKontrol.nudSpratnost.Value,
                            nekretninaKontrol.rchKratakOpis.Text,
                            ((Agent)nekretninaKontrol.cbxAgent.SelectedItem).Id,
                            ((Klijent)nekretninaKontrol.cbxVlasnik.SelectedItem).ID, (int)nudMaxMeseci.Value, txtIznajmljivac.Text);
                    }
                }
                else
                {
                    try
                    {
                        DTOManager.DodajNekretninuZaProdaju
                            (nekretninaKontrol.txtUlica.Text, nekretninaKontrol.txtBrojUUlici.Text,
                            nekretninaKontrol.txtGrad.Text, nekretninaKontrol.cbxTip.SelectedItem.ToString(),
                            (int)nekretninaKontrol.nudKvadratura.Value, nekretninaKontrol.txtBrojKatParcele.Text,
                            nekretninaKontrol.txtKatOpstina.Text, (int)nekretninaKontrol.nudCena.Value,
                            nekretninaKontrol.dtpDatumIzgradnje.Value, (int)nekretninaKontrol.nudSpratnost.Value,
                            nekretninaKontrol.rchKratakOpis.Text,
                            ((Agent)nekretninaKontrol.cbxAgent.SelectedItem).Id,
                            ((Klijent)nekretninaKontrol.cbxVlasnik.SelectedItem).ID);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Greska prilikom dodavanja u bazu\n" + exc.Message);
                    }
                }
            }
            else
            {
                if (cbxTip.SelectedIndex == 0)
                {
                    if (cbxVrstaIznajmljivanja.SelectedIndex == 0)
                    {
                        DTOManager.UpdateNekretnina()
                    }
                    else
                    {
                        DTOManager.DodajNekretninuZaIznajmljivanjeDuzi(nekretninaKontrol.txtUlica.Text, nekretninaKontrol.txtBrojUUlici.Text,
                            nekretninaKontrol.txtGrad.Text, nekretninaKontrol.cbxTip.SelectedItem.ToString(),
                            (int)nekretninaKontrol.nudKvadratura.Value, nekretninaKontrol.txtBrojKatParcele.Text,
                            nekretninaKontrol.txtKatOpstina.Text, (int)nekretninaKontrol.nudCena.Value,
                            nekretninaKontrol.dtpDatumIzgradnje.Value, (int)nekretninaKontrol.nudSpratnost.Value,
                            nekretninaKontrol.rchKratakOpis.Text,
                            ((Agent)nekretninaKontrol.cbxAgent.SelectedItem).Id,
                            ((Klijent)nekretninaKontrol.cbxVlasnik.SelectedItem).ID, (int)nudMaxMeseci.Value, txtIznajmljivac.Text);
                    }
                }
                else
                {
                    try
                    {
                        DTOManager.DodajNekretninuZaProdaju
                            (nekretninaKontrol.txtUlica.Text, nekretninaKontrol.txtBrojUUlici.Text,
                            nekretninaKontrol.txtGrad.Text, nekretninaKontrol.cbxTip.SelectedItem.ToString(),
                            (int)nekretninaKontrol.nudKvadratura.Value, nekretninaKontrol.txtBrojKatParcele.Text,
                            nekretninaKontrol.txtKatOpstina.Text, (int)nekretninaKontrol.nudCena.Value,
                            nekretninaKontrol.dtpDatumIzgradnje.Value, (int)nekretninaKontrol.nudSpratnost.Value,
                            nekretninaKontrol.rchKratakOpis.Text,
                            ((Agent)nekretninaKontrol.cbxAgent.SelectedItem).Id,
                            ((Klijent)nekretninaKontrol.cbxVlasnik.SelectedItem).ID);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Greska prilikom dodavanja u bazu\n" + exc.Message);
                    }
                }
            }
        }
    }
}
