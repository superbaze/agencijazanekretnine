﻿namespace AgencijaZaNekretnine.Forme
{
    partial class NekretninaKontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUlica = new System.Windows.Forms.Label();
            this.lblBrojUULici = new System.Windows.Forms.Label();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblKvadratura = new System.Windows.Forms.Label();
            this.lblKatOpstina = new System.Windows.Forms.Label();
            this.lblCena = new System.Windows.Forms.Label();
            this.lblVlasnik = new System.Windows.Forms.Label();
            this.lblKratakOpis = new System.Windows.Forms.Label();
            this.lblSpratnost = new System.Windows.Forms.Label();
            this.lblDatumIzgradnje = new System.Windows.Forms.Label();
            this.lblBrojKatParcele = new System.Windows.Forms.Label();
            this.lblTip = new System.Windows.Forms.Label();
            this.txtGrad = new System.Windows.Forms.TextBox();
            this.txtUlica = new System.Windows.Forms.TextBox();
            this.txtBrojUUlici = new System.Windows.Forms.TextBox();
            this.txtKatOpstina = new System.Windows.Forms.TextBox();
            this.txtBrojKatParcele = new System.Windows.Forms.TextBox();
            this.dtpDatumIzgradnje = new System.Windows.Forms.DateTimePicker();
            this.cbxTip = new System.Windows.Forms.ComboBox();
            this.nudSpratnost = new System.Windows.Forms.NumericUpDown();
            this.nudKvadratura = new System.Windows.Forms.NumericUpDown();
            this.lbxFotografije = new System.Windows.Forms.ListBox();
            this.nudCena = new System.Windows.Forms.NumericUpDown();
            this.rchKratakOpis = new System.Windows.Forms.RichTextBox();
            this.lblAgent = new System.Windows.Forms.Label();
            this.cbxVlasnik = new System.Windows.Forms.ComboBox();
            this.cbxAgent = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpratnost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKvadratura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCena)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUlica
            // 
            this.lblUlica.AutoSize = true;
            this.lblUlica.Location = new System.Drawing.Point(11, 43);
            this.lblUlica.Name = "lblUlica";
            this.lblUlica.Size = new System.Drawing.Size(34, 13);
            this.lblUlica.TabIndex = 0;
            this.lblUlica.Text = "Ulica:";
            // 
            // lblBrojUULici
            // 
            this.lblBrojUULici.AutoSize = true;
            this.lblBrojUULici.Location = new System.Drawing.Point(11, 69);
            this.lblBrojUULici.Name = "lblBrojUULici";
            this.lblBrojUULici.Size = new System.Drawing.Size(58, 13);
            this.lblBrojUULici.TabIndex = 1;
            this.lblBrojUULici.Text = "Broj u ulici:";
            // 
            // lblGrad
            // 
            this.lblGrad.AutoSize = true;
            this.lblGrad.Location = new System.Drawing.Point(11, 17);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(33, 13);
            this.lblGrad.TabIndex = 2;
            this.lblGrad.Text = "Grad:";
            // 
            // lblKvadratura
            // 
            this.lblKvadratura.AutoSize = true;
            this.lblKvadratura.Location = new System.Drawing.Point(11, 147);
            this.lblKvadratura.Name = "lblKvadratura";
            this.lblKvadratura.Size = new System.Drawing.Size(62, 13);
            this.lblKvadratura.TabIndex = 3;
            this.lblKvadratura.Text = "Kvadratura:";
            // 
            // lblKatOpstina
            // 
            this.lblKatOpstina.AutoSize = true;
            this.lblKatOpstina.Location = new System.Drawing.Point(11, 226);
            this.lblKatOpstina.Name = "lblKatOpstina";
            this.lblKatOpstina.Size = new System.Drawing.Size(66, 13);
            this.lblKatOpstina.TabIndex = 4;
            this.lblKatOpstina.Text = "Kat. opstina:";
            // 
            // lblCena
            // 
            this.lblCena.AutoSize = true;
            this.lblCena.Location = new System.Drawing.Point(11, 173);
            this.lblCena.Name = "lblCena";
            this.lblCena.Size = new System.Drawing.Size(35, 13);
            this.lblCena.TabIndex = 5;
            this.lblCena.Text = "Cena:";
            // 
            // lblVlasnik
            // 
            this.lblVlasnik.AutoSize = true;
            this.lblVlasnik.Location = new System.Drawing.Point(11, 278);
            this.lblVlasnik.Name = "lblVlasnik";
            this.lblVlasnik.Size = new System.Drawing.Size(44, 13);
            this.lblVlasnik.TabIndex = 11;
            this.lblVlasnik.Text = "Vlasnik:";
            // 
            // lblKratakOpis
            // 
            this.lblKratakOpis.AutoSize = true;
            this.lblKratakOpis.Location = new System.Drawing.Point(11, 336);
            this.lblKratakOpis.Name = "lblKratakOpis";
            this.lblKratakOpis.Size = new System.Drawing.Size(63, 13);
            this.lblKratakOpis.TabIndex = 10;
            this.lblKratakOpis.Text = "Kratak opis:";
            // 
            // lblSpratnost
            // 
            this.lblSpratnost.AutoSize = true;
            this.lblSpratnost.Location = new System.Drawing.Point(11, 121);
            this.lblSpratnost.Name = "lblSpratnost";
            this.lblSpratnost.Size = new System.Drawing.Size(55, 13);
            this.lblSpratnost.TabIndex = 9;
            this.lblSpratnost.Text = "Spratnost:";
            // 
            // lblDatumIzgradnje
            // 
            this.lblDatumIzgradnje.AutoSize = true;
            this.lblDatumIzgradnje.Location = new System.Drawing.Point(11, 203);
            this.lblDatumIzgradnje.Name = "lblDatumIzgradnje";
            this.lblDatumIzgradnje.Size = new System.Drawing.Size(86, 13);
            this.lblDatumIzgradnje.TabIndex = 8;
            this.lblDatumIzgradnje.Text = "Datum izgradnje:";
            // 
            // lblBrojKatParcele
            // 
            this.lblBrojKatParcele.AutoSize = true;
            this.lblBrojKatParcele.Location = new System.Drawing.Point(11, 252);
            this.lblBrojKatParcele.Name = "lblBrojKatParcele";
            this.lblBrojKatParcele.Size = new System.Drawing.Size(87, 13);
            this.lblBrojKatParcele.TabIndex = 7;
            this.lblBrojKatParcele.Text = "Broj kat. parcele:";
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(11, 95);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(25, 13);
            this.lblTip.TabIndex = 6;
            this.lblTip.Text = "Tip:";
            // 
            // txtGrad
            // 
            this.txtGrad.Location = new System.Drawing.Point(127, 14);
            this.txtGrad.Name = "txtGrad";
            this.txtGrad.Size = new System.Drawing.Size(127, 20);
            this.txtGrad.TabIndex = 13;
            // 
            // txtUlica
            // 
            this.txtUlica.Location = new System.Drawing.Point(127, 40);
            this.txtUlica.Name = "txtUlica";
            this.txtUlica.Size = new System.Drawing.Size(127, 20);
            this.txtUlica.TabIndex = 14;
            // 
            // txtBrojUUlici
            // 
            this.txtBrojUUlici.Location = new System.Drawing.Point(127, 66);
            this.txtBrojUUlici.Name = "txtBrojUUlici";
            this.txtBrojUUlici.Size = new System.Drawing.Size(127, 20);
            this.txtBrojUUlici.TabIndex = 15;
            // 
            // txtKatOpstina
            // 
            this.txtKatOpstina.Location = new System.Drawing.Point(127, 223);
            this.txtKatOpstina.Name = "txtKatOpstina";
            this.txtKatOpstina.Size = new System.Drawing.Size(127, 20);
            this.txtKatOpstina.TabIndex = 22;
            // 
            // txtBrojKatParcele
            // 
            this.txtBrojKatParcele.Location = new System.Drawing.Point(127, 249);
            this.txtBrojKatParcele.Name = "txtBrojKatParcele";
            this.txtBrojKatParcele.Size = new System.Drawing.Size(127, 20);
            this.txtBrojKatParcele.TabIndex = 23;
            // 
            // dtpDatumIzgradnje
            // 
            this.dtpDatumIzgradnje.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatumIzgradnje.Location = new System.Drawing.Point(127, 197);
            this.dtpDatumIzgradnje.Name = "dtpDatumIzgradnje";
            this.dtpDatumIzgradnje.Size = new System.Drawing.Size(127, 20);
            this.dtpDatumIzgradnje.TabIndex = 26;
            // 
            // cbxTip
            // 
            this.cbxTip.FormattingEnabled = true;
            this.cbxTip.Location = new System.Drawing.Point(127, 92);
            this.cbxTip.Name = "cbxTip";
            this.cbxTip.Size = new System.Drawing.Size(127, 21);
            this.cbxTip.TabIndex = 27;
            // 
            // nudSpratnost
            // 
            this.nudSpratnost.Location = new System.Drawing.Point(127, 119);
            this.nudSpratnost.Name = "nudSpratnost";
            this.nudSpratnost.Size = new System.Drawing.Size(127, 20);
            this.nudSpratnost.TabIndex = 28;
            // 
            // nudKvadratura
            // 
            this.nudKvadratura.Location = new System.Drawing.Point(127, 145);
            this.nudKvadratura.Name = "nudKvadratura";
            this.nudKvadratura.Size = new System.Drawing.Size(127, 20);
            this.nudKvadratura.TabIndex = 29;
            // 
            // lbxFotografije
            // 
            this.lbxFotografije.FormattingEnabled = true;
            this.lbxFotografije.Location = new System.Drawing.Point(287, 14);
            this.lbxFotografije.Name = "lbxFotografije";
            this.lbxFotografije.Size = new System.Drawing.Size(252, 433);
            this.lbxFotografije.TabIndex = 30;
            // 
            // nudCena
            // 
            this.nudCena.Location = new System.Drawing.Point(127, 171);
            this.nudCena.Name = "nudCena";
            this.nudCena.Size = new System.Drawing.Size(127, 20);
            this.nudCena.TabIndex = 31;
            // 
            // rchKratakOpis
            // 
            this.rchKratakOpis.Location = new System.Drawing.Point(14, 352);
            this.rchKratakOpis.Name = "rchKratakOpis";
            this.rchKratakOpis.Size = new System.Drawing.Size(267, 96);
            this.rchKratakOpis.TabIndex = 32;
            this.rchKratakOpis.Text = "";
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Location = new System.Drawing.Point(11, 305);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(38, 13);
            this.lblAgent.TabIndex = 33;
            this.lblAgent.Text = "Agent:";
            // 
            // cbxVlasnik
            // 
            this.cbxVlasnik.FormattingEnabled = true;
            this.cbxVlasnik.Location = new System.Drawing.Point(127, 275);
            this.cbxVlasnik.Name = "cbxVlasnik";
            this.cbxVlasnik.Size = new System.Drawing.Size(127, 21);
            this.cbxVlasnik.TabIndex = 34;
            // 
            // cbxAgent
            // 
            this.cbxAgent.FormattingEnabled = true;
            this.cbxAgent.Location = new System.Drawing.Point(127, 302);
            this.cbxAgent.Name = "cbxAgent";
            this.cbxAgent.Size = new System.Drawing.Size(127, 21);
            this.cbxAgent.TabIndex = 35;
            // 
            // NekretninaKontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbxAgent);
            this.Controls.Add(this.cbxVlasnik);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.rchKratakOpis);
            this.Controls.Add(this.nudCena);
            this.Controls.Add(this.lbxFotografije);
            this.Controls.Add(this.nudKvadratura);
            this.Controls.Add(this.nudSpratnost);
            this.Controls.Add(this.cbxTip);
            this.Controls.Add(this.dtpDatumIzgradnje);
            this.Controls.Add(this.txtBrojKatParcele);
            this.Controls.Add(this.txtKatOpstina);
            this.Controls.Add(this.txtBrojUUlici);
            this.Controls.Add(this.txtUlica);
            this.Controls.Add(this.txtGrad);
            this.Controls.Add(this.lblVlasnik);
            this.Controls.Add(this.lblKratakOpis);
            this.Controls.Add(this.lblSpratnost);
            this.Controls.Add(this.lblDatumIzgradnje);
            this.Controls.Add(this.lblBrojKatParcele);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.lblCena);
            this.Controls.Add(this.lblKatOpstina);
            this.Controls.Add(this.lblKvadratura);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.lblBrojUULici);
            this.Controls.Add(this.lblUlica);
            this.Name = "NekretninaKontrol";
            this.Size = new System.Drawing.Size(574, 467);
            ((System.ComponentModel.ISupportInitialize)(this.nudSpratnost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKvadratura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCena)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUlica;
        private System.Windows.Forms.Label lblBrojUULici;
        private System.Windows.Forms.Label lblGrad;
        private System.Windows.Forms.Label lblKvadratura;
        private System.Windows.Forms.Label lblKatOpstina;
        private System.Windows.Forms.Label lblCena;
        private System.Windows.Forms.Label lblVlasnik;
        private System.Windows.Forms.Label lblKratakOpis;
        private System.Windows.Forms.Label lblSpratnost;
        private System.Windows.Forms.Label lblDatumIzgradnje;
        private System.Windows.Forms.Label lblBrojKatParcele;
        private System.Windows.Forms.Label lblTip;
        public System.Windows.Forms.TextBox txtGrad;
        public System.Windows.Forms.TextBox txtUlica;
        public System.Windows.Forms.TextBox txtBrojUUlici;
        public System.Windows.Forms.TextBox txtKatOpstina;
        public System.Windows.Forms.TextBox txtBrojKatParcele;
        public System.Windows.Forms.DateTimePicker dtpDatumIzgradnje;
        public System.Windows.Forms.ComboBox cbxTip;
        public System.Windows.Forms.NumericUpDown nudSpratnost;
        public System.Windows.Forms.NumericUpDown nudKvadratura;
        public System.Windows.Forms.ListBox lbxFotografije;
        public System.Windows.Forms.NumericUpDown nudCena;
        public System.Windows.Forms.RichTextBox rchKratakOpis;
        private System.Windows.Forms.Label lblAgent;
        public System.Windows.Forms.ComboBox cbxVlasnik;
        public System.Windows.Forms.ComboBox cbxAgent;
    }
}
