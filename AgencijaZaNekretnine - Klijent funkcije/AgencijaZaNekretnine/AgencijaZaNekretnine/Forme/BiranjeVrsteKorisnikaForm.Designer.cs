﻿namespace AgencijaZaNekretnine
{
    partial class BiranjeVrsteKorisnikaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKlijent = new System.Windows.Forms.Button();
            this.btnZaposleni = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnKlijent
            // 
            this.btnKlijent.Location = new System.Drawing.Point(148, 26);
            this.btnKlijent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnKlijent.Name = "btnKlijent";
            this.btnKlijent.Size = new System.Drawing.Size(159, 46);
            this.btnKlijent.TabIndex = 0;
            this.btnKlijent.Text = "Klijent";
            this.btnKlijent.UseVisualStyleBackColor = true;
            this.btnKlijent.Click += new System.EventHandler(this.btnKlijent_Click);
            // 
            // btnZaposleni
            // 
            this.btnZaposleni.Location = new System.Drawing.Point(148, 79);
            this.btnZaposleni.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnZaposleni.Name = "btnZaposleni";
            this.btnZaposleni.Size = new System.Drawing.Size(159, 46);
            this.btnZaposleni.TabIndex = 1;
            this.btnZaposleni.Text = "Zaposleni";
            this.btnZaposleni.UseVisualStyleBackColor = true;
            this.btnZaposleni.Click += new System.EventHandler(this.btnZaposleni_Click);
            // 
            // BiranjeVrsteKorisnikaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 139);
            this.Controls.Add(this.btnZaposleni);
            this.Controls.Add(this.btnKlijent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(458, 186);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(458, 186);
            this.Name = "BiranjeVrsteKorisnikaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BiranjeVrsteKorisnikaForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnKlijent;
        private System.Windows.Forms.Button btnZaposleni;
    }
}