﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    public class BrojTelefonaMapiranje:ClassMap<BrojTelefona>
    {
        public BrojTelefonaMapiranje()
        {
            Table("BROJ_TELEFONA");

            CompositeId()
                .KeyProperty(x => x.BrTelefona,"BROJ_TELEFONA")
                .KeyReference(x => x.IdAgenta,"ID_AGENTA");

        }
    }
}
