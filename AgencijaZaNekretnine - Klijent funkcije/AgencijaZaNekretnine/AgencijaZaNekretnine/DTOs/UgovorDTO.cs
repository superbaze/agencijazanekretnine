﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgencijaZaNekretnine.Entiteti;

namespace AgencijaZaNekretnine.DTOs
{
    public class UgovorDTO
    {
        public int Id { get; set; }
        public DateTime DatumPotpisivanja { get; set; }
        public float NaknadaZaPosredovanje { get; set; }
        public float BonusZaAgenta { get; set; }
        public string Tip { get; set; }

        public UgovorDTO() { }

        public UgovorDTO(Ugovor ugovor)
        {
            Id = ugovor.Broj;
            DatumPotpisivanja = ugovor.DatumPotpisivanja;
            NaknadaZaPosredovanje = ugovor.NaknadaZaPosredovanje;
            BonusZaAgenta = ugovor.BonusZaAgenta;
            if (ugovor is UgovorZaIznajmljivanje)
                Tip = "Iznajmljivanje";
            else if (ugovor is UgovorZaProdaju)
                Tip = "Prodaja";
        }
    }
}
