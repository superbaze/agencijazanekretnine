﻿namespace AgencijaZaNekretnine.Entiteti
{
    public class FotografijaID
    {
        public virtual int Id { get; set; }
        public virtual Nekretnina Nekretnina { get; set; }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(FotografijaID))
                return false;

            FotografijaID recvObj = (FotografijaID)obj;

            if ((Id == recvObj.Id) && (Nekretnina.Id == recvObj.Nekretnina.Id))
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}