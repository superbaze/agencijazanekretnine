﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public abstract class NekretninaZaIznajmljivanje : Nekretnina
    {
        public virtual IList<UgovorZaIznajmljivanje> Ugovori { get; set; }

        public NekretninaZaIznajmljivanje()
        {
            Ugovori = new List<UgovorZaIznajmljivanje>();
            this.ZaProdaju = "ne";
        }
    }

    public class NekretninaNaKraciPeriod: NekretninaZaIznajmljivanje
    {
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }

        public NekretninaNaKraciPeriod()
            :base()
        {
            this.Tip_Za_Iznajmljivanje = "kraci period";
        }
    }

    public class NekretninaNaDuziPeriod : NekretninaZaIznajmljivanje
    {
        public virtual int MaximalniBrojMeseci { get; set; }
        public virtual string Iznajmljivac { get; set; }

        public NekretninaNaDuziPeriod()
            :base()
        {
            this.Tip_Za_Iznajmljivanje = "duzi period";
        }
    }
}
