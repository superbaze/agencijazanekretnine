﻿using AgencijaZaNekretnine.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgencijaZaNekretnine.Forme
{
    public partial class KlijentForm : Form
    {
        private KlijentDTO klijent;
        public KlijentForm()
        {
            InitializeComponent();
        }
        public KlijentForm(KlijentDTO klijent) : this()
        {
            this.klijent = klijent;
            lblIme.Text = klijent.Ime;
            lblPrezime.Text = klijent.Prezime;
            ckbVidiSve_CheckedChanged(null, null);
            OsveziKontrole(); 
        }
        public void OsveziKontrole()
        {
            lbxNekretnine.Items.Clear();
            if (ckbVidiSve.Checked == true)
            {
                lbxNekretnine.Items.AddRange
                    (DTOManager.VratiSveNekretnine().ToArray());

            }
            else if (ckbIznajmioSam.Checked == true)
            {
                lbxNekretnine.Items.AddRange
                    (DTOManager.VratiNekretnineIznajmljene(klijent.Id).ToArray());
            }
            else if (ckbVlasnikSam.Checked == true)
            {
                lbxNekretnine.Items.AddRange
                    (DTOManager.VratiNekretnineVlasnik(klijent.Id).ToArray());
            }
        }

        private void ckbVidiSve_CheckedChanged(object sender, EventArgs e)
        {
            if(ckbVidiSve.Checked == true)
            {
                ckbIznajmioSam.Checked = false;
                ckbVlasnikSam.Checked = false;
                ckbIznajmioSam.Enabled = false;
                ckbVlasnikSam.Enabled = false;
            }
            else
            {
                if (klijent.Kupac == 1)
                    ckbIznajmioSam.Enabled = true;
                if (klijent.Vlasnik == 1)
                    ckbVlasnikSam.Enabled = true;
            }
        }

        private void btnOsvezi_Click(object sender, EventArgs e)
        {
            OsveziKontrole();
        }
    }
}
