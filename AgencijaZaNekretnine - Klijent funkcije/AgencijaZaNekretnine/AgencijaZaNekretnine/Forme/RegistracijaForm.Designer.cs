﻿namespace AgencijaZaNekretnine.Forme
{
    partial class RegistracijaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIme = new System.Windows.Forms.Label();
            this.lblPrezime = new System.Windows.Forms.Label();
            this.lblSifra = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblUlica = new System.Windows.Forms.Label();
            this.lblBrojUUlici = new System.Windows.Forms.Label();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblBrojTelefona = new System.Windows.Forms.Label();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.txtSifra = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtUlica = new System.Windows.Forms.TextBox();
            this.txtBrojUUlici = new System.Windows.Forms.TextBox();
            this.txtGrad = new System.Windows.Forms.TextBox();
            this.txtBrojTelefona = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(8, 9);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(27, 13);
            this.lblIme.TabIndex = 0;
            this.lblIme.Text = "Ime:";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(8, 46);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(47, 13);
            this.lblPrezime.TabIndex = 1;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblSifra
            // 
            this.lblSifra.AutoSize = true;
            this.lblSifra.Location = new System.Drawing.Point(8, 83);
            this.lblSifra.Name = "lblSifra";
            this.lblSifra.Size = new System.Drawing.Size(31, 13);
            this.lblSifra.TabIndex = 2;
            this.lblSifra.Text = "Sifra:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(8, 120);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 3;
            this.lblEmail.Text = "E-mail:";
            // 
            // lblUlica
            // 
            this.lblUlica.AutoSize = true;
            this.lblUlica.Location = new System.Drawing.Point(8, 157);
            this.lblUlica.Name = "lblUlica";
            this.lblUlica.Size = new System.Drawing.Size(34, 13);
            this.lblUlica.TabIndex = 4;
            this.lblUlica.Text = "Ulica:";
            // 
            // lblBrojUUlici
            // 
            this.lblBrojUUlici.AutoSize = true;
            this.lblBrojUUlici.Location = new System.Drawing.Point(8, 194);
            this.lblBrojUUlici.Name = "lblBrojUUlici";
            this.lblBrojUUlici.Size = new System.Drawing.Size(58, 13);
            this.lblBrojUUlici.TabIndex = 5;
            this.lblBrojUUlici.Text = "Broj u ulici:";
            // 
            // lblGrad
            // 
            this.lblGrad.AutoSize = true;
            this.lblGrad.Location = new System.Drawing.Point(8, 231);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(33, 13);
            this.lblGrad.TabIndex = 6;
            this.lblGrad.Text = "Grad:";
            // 
            // lblBrojTelefona
            // 
            this.lblBrojTelefona.AutoSize = true;
            this.lblBrojTelefona.Location = new System.Drawing.Point(8, 268);
            this.lblBrojTelefona.Name = "lblBrojTelefona";
            this.lblBrojTelefona.Size = new System.Drawing.Size(69, 13);
            this.lblBrojTelefona.TabIndex = 7;
            this.lblBrojTelefona.Text = "Broj telefona:";
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(94, 6);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(163, 20);
            this.txtIme.TabIndex = 8;
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(94, 43);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(163, 20);
            this.txtPrezime.TabIndex = 9;
            // 
            // txtSifra
            // 
            this.txtSifra.Location = new System.Drawing.Point(94, 80);
            this.txtSifra.Name = "txtSifra";
            this.txtSifra.PasswordChar = '*';
            this.txtSifra.Size = new System.Drawing.Size(163, 20);
            this.txtSifra.TabIndex = 10;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(94, 117);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(163, 20);
            this.txtEmail.TabIndex = 11;
            // 
            // txtUlica
            // 
            this.txtUlica.Location = new System.Drawing.Point(94, 154);
            this.txtUlica.Name = "txtUlica";
            this.txtUlica.Size = new System.Drawing.Size(163, 20);
            this.txtUlica.TabIndex = 12;
            // 
            // txtBrojUUlici
            // 
            this.txtBrojUUlici.Location = new System.Drawing.Point(94, 191);
            this.txtBrojUUlici.Name = "txtBrojUUlici";
            this.txtBrojUUlici.Size = new System.Drawing.Size(163, 20);
            this.txtBrojUUlici.TabIndex = 13;
            // 
            // txtGrad
            // 
            this.txtGrad.Location = new System.Drawing.Point(94, 228);
            this.txtGrad.Name = "txtGrad";
            this.txtGrad.Size = new System.Drawing.Size(163, 20);
            this.txtGrad.TabIndex = 14;
            // 
            // txtBrojTelefona
            // 
            this.txtBrojTelefona.Location = new System.Drawing.Point(94, 265);
            this.txtBrojTelefona.Name = "txtBrojTelefona";
            this.txtBrojTelefona.Size = new System.Drawing.Size(163, 20);
            this.txtBrojTelefona.TabIndex = 15;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(182, 307);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 16;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // RegistracijaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 342);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtBrojTelefona);
            this.Controls.Add(this.txtGrad);
            this.Controls.Add(this.txtBrojUUlici);
            this.Controls.Add(this.txtUlica);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtSifra);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.lblBrojTelefona);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.lblBrojUUlici);
            this.Controls.Add(this.lblUlica);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblSifra);
            this.Controls.Add(this.lblPrezime);
            this.Controls.Add(this.lblIme);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RegistracijaForm";
            this.Text = "RegistracijaForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblPrezime;
        private System.Windows.Forms.Label lblSifra;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblUlica;
        private System.Windows.Forms.Label lblBrojUUlici;
        private System.Windows.Forms.Label lblGrad;
        private System.Windows.Forms.Label lblBrojTelefona;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.TextBox txtSifra;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtUlica;
        private System.Windows.Forms.TextBox txtBrojUUlici;
        private System.Windows.Forms.TextBox txtGrad;
        private System.Windows.Forms.TextBox txtBrojTelefona;
        private System.Windows.Forms.Button btnOK;
    }
}