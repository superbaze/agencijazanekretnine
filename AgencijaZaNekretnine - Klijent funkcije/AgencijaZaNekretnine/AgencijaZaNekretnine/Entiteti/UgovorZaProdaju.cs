﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Entiteti
{
    public class UgovorZaProdaju : Ugovor
    {
        public virtual float OstvarenaCena { get; set; }
        public virtual float NaknadaZaNotara { get; set; }
        public virtual string ImeNotara { get; set; }
        public virtual NekretninaZaProdaju Prodaja { get; set; }
    }
}
