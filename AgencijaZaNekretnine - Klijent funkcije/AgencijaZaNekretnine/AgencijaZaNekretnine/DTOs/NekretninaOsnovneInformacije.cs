﻿using AgencijaZaNekretnine.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.DTOs
{
    public class NekretninaOsnovneInformacije
    {
        public int Id { get; set; }
        public string Ulica { get; set; }
        public string BrojUUlici { get; set; }
        public string GradLokacija { get; set; }
        public string Tip { get; set; }
        public string KratakOpis { get; set; }
        public string TipNekretnine { get; set; }

        public NekretninaOsnovneInformacije()
        {

        }

        public NekretninaOsnovneInformacije(Nekretnina nekretnina)
        {
            this.Id = nekretnina.Id;
            this.GradLokacija = nekretnina.GradLokacija;
            this.KratakOpis = nekretnina.KratakOpis;
            this.Ulica = nekretnina.Ulica;
            this.BrojUUlici = nekretnina.BrojUUlici;
            this.Tip = nekretnina.Tip;
            if (typeof(NekretninaNaDuziPeriod) == nekretnina.GetType())
                this.TipNekretnine = "iznajmljivanje - duzi period";
            else if (typeof(NekretninaNaKraciPeriod) == nekretnina.GetType())
                this.TipNekretnine = "iznajmljivanje - kraci period";
            else if (typeof(NekretninaZaProdaju) == nekretnina.GetType())
                this.TipNekretnine = "prodaja";
        }

        public override string ToString()
        {
            return GradLokacija + "   " +  Ulica + "  " + BrojUUlici  + "  " + Tip;
        }
    }
}
