﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class NekretninaZaProdajuMapiranja : SubclassMap<NekretninaZaProdaju>
    {
        public NekretninaZaProdajuMapiranja()
        {
            HasOne(x => x.Ugovor).PropertyRef(x => x.Prodaja);
        }
    }
}
