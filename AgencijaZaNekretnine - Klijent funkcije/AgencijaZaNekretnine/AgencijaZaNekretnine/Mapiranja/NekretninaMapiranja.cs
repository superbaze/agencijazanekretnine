﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    class NekretninaMapiranja : ClassMap<Nekretnina>
    {
        public NekretninaMapiranja()
        {
            Table("NEKRETNINA");

            DiscriminateSubClassesOnColumn("")
                .Formula("CASE WHEN (ZAPRODAJU = 'da') THEN 'NekretninaZaProdaju'" +
                " WHEN (ZAPRODAJU = 'ne' AND TIP_ZA_IZNAJMLJIVANJE='kraci period') THEN 'NekretninaNaKraciPeriod' " +
                "WHEN (ZAPRODAJU = 'ne' AND TIP_ZA_IZNAJMLJIVANJE='duzi period') THEN 'NekretninaNaDuziPeriod' END");
               
            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            Map(x => x.Ulica, "ULICA");
            Map(x => x.BrojUUlici, "BROJ_U_ULICI");
            Map(x => x.GradLokacija, "GRAD_LOKACIJA");
            Map(x => x.Tip, "TIP");
            Map(x => x.Kvadratura, "KVADRATURA");
            Map(x => x.BrojKatastarskeParcele, "BROJ_KATASTARSKE_PARCELE");
            Map(x => x.KatastarskaOpstina, "KATASTARSKA_OPSTINA");
            Map(x => x.Cena, "CENA");
            Map(x => x.DatumIzgradnje, "DATUM_IZGRADNJE");
            Map(x => x.Spratnost, "SPRATNOST");
            Map(x => x.KratakOpis, "KRATAK_OPIS");
            Map(x => x.IdFizickogUgovora, "ID_FIZICKOG_UGOVORA");
            Map(x => x.ZaProdaju, "ZAPRODAJU");
            Map(x => x.Tip_Za_Iznajmljivanje, "TIP_ZA_IZNAJMLJIVANJE");
  
            References(x => x.PripadaAgentu).Column("ID_AGENTA").LazyLoad();
            References(x => x.PripadaVlasniku).Column("ID_VLASNIKA").LazyLoad();

            HasMany(x => x.Fotografije).KeyColumn("ID_NEKRETNINE").LazyLoad().Inverse().Cascade.All();

        }
    }
}
