﻿using AgencijaZaNekretnine.Entiteti;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.Mapiranja
{
    public class FotografijaMapiranje: ClassMap<Fotografija>
    {
        public FotografijaMapiranje()
        {
            Table("FOTOGRAFIJA");

            CompositeId(x => x.id)
                .KeyReference(x => x.Nekretnina, "ID_NEKRETNINE")
                .KeyProperty(x => x.Id, "ID");
            Map(x => x.Sadrzaj, "SADRZAJ");
        }
    }
}
