﻿using AgencijaZaNekretnine.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgencijaZaNekretnine.DTOs
{
    public class KlijentDTO
    {
        private String ime;
        private String prezime;
        private int id;
        private int vlasnik;
        private int kupac;

        public KlijentDTO(Klijent k)
        {
            this.ime = k.Ime;
            this.prezime = k.Prezime;
            this.id = k.ID;
            this.kupac = k.JeKupac;
            this.vlasnik = k.JeVlasnik;
        }

        public string Ime { get => ime; }
        public string Prezime { get => prezime;}
        public int Id { get => id; }
        public int Vlasnik { get => vlasnik; }
        public int Kupac { get => kupac; }
    }
}
